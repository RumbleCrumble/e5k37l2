package com.thearchon.sgs.handlers;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.thearchon.sgs.Core;

public class ThePlayer {

	Core plugin = Core.getCore();
	
	String name;
	
	UUID id;
	
	int points;
	int league;
	int wins;
	int losses;
	int kills;
	int ranking;
	int coins;
	
	boolean admin;
	
	Player p;
	
	
	
	public ThePlayer(Player p){
		this.name = p.getName();
		this.id = p.getUniqueId();
		this.p = p;
		
		//this.wins = plugin.getSQL().getWins(p);
		//this.losses = plugin.getSQL().getLosses(p);
		//this.kills = plugin.getSQL().getKills(p);
		
		this.wins = plugin.getSQL().wins.get(p);
		this.losses = plugin.getSQL().losses.get(p);
		this.kills = plugin.getSQL().kills.get(p);
		this.coins = plugin.getSQL().coins.get(p);
		
		this.points = this.kills * 100 + this.wins * 1250 + this.losses * -50;
	
		if (this.points >= 250000){ // 250k+ diamond
			this.league = 4;
		}
		
		if (this.points <= 250000 && this.points >= 100000){ // 250k gold
			this.league = 3;
		}
		
		if (this.points <= 100000 && this.points >= 25000){ // 100k iron
			this.league = 2;
		}
		
		if (this.points <= 25000){ // 25k leather
			this.league = 1;
		}
		
		if (p.isOp()){
			this.admin = true;
		}
		
		this.ranking = plugin.getSQL().getRanking(p);
		
	}
	
	public int getCoins(){
		return this.coins;
	}
	
	public String getName(){
		return this.name;
	}
	
	public UUID getID(){
		return this.id;
	}
	
	public boolean isAdmin(){
		return this.admin;
	}
	
	public String getLeague(){ 
		
		
		if (this.league == 1){
			return "§7§lLeather";
		}
		
		if (this.league == 2){
			return "§f§lIron";
		}
		
		if (this.league == 3){
			return "§6§lGold";
		}
		
		if (this.league == 4){
			return "§b§lDiamond";
		}
		
		return "§7§lLeather";

	}
	
	public Player getPlayer(){
		return Bukkit.getPlayer(this.id);
	}
	
	public void sendMessage(String msg){
		this.p.sendMessage(msg);
	}
	
	public void spawn(){
		this.p.teleport(new Location(Bukkit.getWorld("world"), -1227, 4, 1460));
	}
	
	public int getPoints(){
		return this.points;
	}
	
	public int getKills(){
		return this.kills;
	}
	
	public int getWins(){
		return this.wins;
	}
	
	public int getLosses(){
		return this.losses;
	}
	
	public int getRanking(){
		return this.ranking;
	}

	
}

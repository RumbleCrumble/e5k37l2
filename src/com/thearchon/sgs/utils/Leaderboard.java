package com.thearchon.sgs.utils;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.thearchon.sgs.Core;

public class Leaderboard {

	static Core plugin = Core.getCore();
	
	public static void startHeadStuff(){
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

			@Override
			public void run() {
				Location loc = new Location(Bukkit.getWorld("world"), -100, 65, -508);
				
				Block b = loc.getBlock();
				
				ItemStack skull = new ItemStack(b.getType(), (byte) 3);
				
				SkullMeta sm = (SkullMeta) skull.getItemMeta();
				
				sm.setOwner("TheTrollzYT");
				
				skull.setItemMeta(sm);
				
				
			}
			
			
		}, 0, 60 * 20);
	}
	
}

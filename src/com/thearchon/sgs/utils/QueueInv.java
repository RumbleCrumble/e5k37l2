package com.thearchon.sgs.utils;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class QueueInv {

	public void inv(Player p){
		Inventory inv = Bukkit.createInventory(null, 9, "§c§nSurvival Games Menu");
		
		ItemStack one = new ItemStack(Material.EMERALD);
		ItemStack jq = new ItemStack(Material.EMERALD_BLOCK);
		ItemStack lq = new ItemStack(Material.REDSTONE_BLOCK);
		ItemStack pl = new ItemStack(Material.STAINED_GLASS_PANE);
		
		ItemMeta oneMeta = one.getItemMeta();
		ItemMeta jqMeta = jq.getItemMeta();
		ItemMeta lqMeta = lq.getItemMeta();
		ItemMeta plMeta = pl.getItemMeta();
		
		oneMeta.setDisplayName("§aSG Shop");
		jqMeta.setDisplayName("§aJoin Queue");
		lqMeta.setDisplayName("§cLeave Queue");
		plMeta.setDisplayName("|");
		
		oneMeta.setLore(Arrays.asList("§9Coming Soon!"));
		jqMeta.setLore(Arrays.asList("§9Click to join the Survival Games queue."));
		lqMeta.setLore(Arrays.asList("§9Click to leave the Survival Games queue."));
		
		one.setItemMeta(oneMeta);
		jq.setItemMeta(jqMeta);
		lq.setItemMeta(lqMeta);
		pl.setItemMeta(plMeta);
		
		inv.setItem(0, pl);
		inv.setItem(1, one);
		inv.setItem(2, pl);
		inv.setItem(3, jq);
		inv.setItem(4, pl);
		inv.setItem(5, lq);
		inv.setItem(6, pl);
		inv.setItem(7, one);
		inv.setItem(8, pl);
		
		p.openInventory(inv);
	}
	
}

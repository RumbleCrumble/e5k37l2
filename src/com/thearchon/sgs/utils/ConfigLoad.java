package com.thearchon.sgs.utils;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import com.thearchon.sgs.Core;

public class ConfigLoad 
{
	static Plugin plugin = Core.instance;
	
	public static void loadConfiguration(){
	     String[] list1 = {  "Did I win?",
							 "Am I good yet?",
							 "Im montaging that",
							 "Too rek or not to rek, that is the question",
							 "You can watch that amazing victory on my youtube channel, 'Technoblade'",
							 "You can watch that amazing victory on my youtube channel, 'RumbleCrumble'",
							 "You can watch that amazing victory on my youtube channel, 'Huahwi'",
							 "You can watch that amazing victory on my youtube channel, 'Fashion101'",
							 "You can watch that amazing victory on my youtube channel, 'Pewdiepie'",
							 "You can watch that amazing victory on my youtube channel, 'SkyDoesMinecraft'",
							 "Sometimes its tough being the best",
							 "Haters gonna hate hate hate hate hate",
							 "They even nerfed me the other day too lmao!",
							 "Im going back to real SG on mineplex cuz this is just too eZ!",
							 "I remember when I lossed once once",
							 "Id like to thank MattyBRaps for the inspiration music right before this game",
							 "Good fight, try harder next time tho",
							 "Is this real life?",
							 "Wow, camping really does work",
							 "Yusssssss",
							 "Oh gg, I was lagging hardcore",
							 "Id like to thank my ping for this victory",
							 "Im best of the best!",
							 "Dammit those hax were a little too obvious",
							 "This was my FaZe application",
							 "This will make for a perfect meme",
							 "Dudududududududu"};
	     plugin.getConfig().set("prizeBox.winMessage.common", Arrays.asList(list1));
	     
	     String[] list2 = { "Players gonna play play play play play",
							"Shake it off, shake it off!",
							"I just katniss everdeened your bootay",
							"Ha, I even volunteered as tribute!",
							"Ya Im pretty skewl irl",
							"Its a me xXFaZe_360MLGquickscoperXx",
							"Thats how you rek a team!",
							"I did it 4 the vine",
							"9+10 = 21 victories for me!"};
	     plugin.getConfig().set("prizeBox.winMessage.rare", Arrays.asList(list2));
	     
	     String[] list3 = { "GG",
							"eZpZ",
							"Yes, I finally won!",
							"1 win away from 69!",									
							"Id like to thank skewl for teaching me how to get g00d",
							"1v1 m8?",
							"3 eZ 5 me",
							"Flawlessed!",
							"Pull-through god",
							"You just got rick rolled!"};
	     plugin.getConfig().set("prizeBox.winMessage.mythical", Arrays.asList(list3));
	     
	     String[] list4 = { "Gotta go fast!",
							"Falco Punch!",
							"My win/loss ratio is now 6/9",
							"360 noscoooped"};
	     plugin.getConfig().set("prizeBox.winMessage.legendary", Arrays.asList(list4));
	     
	     String[] list5 = { "I blame Obama for this defeat.",
							"It was probably your ping.",
							"Nice hacks!",
							"My little brother was playing",
							"Reported",
							"Why didnt my aimbot turn!",
							"Jitterclicker",
							"I was lagging!",
							"I just started playing MC the other day :/",
							"Im only 8",
							"Im telling my parents for being r00d",
							"My hands were cold",
							"Its never ogre",
							"Nice targeting noob",
							"Im calling my mom!",
							"I was AFK!!",
							"Consoles are better than PCs anyways",
							"My laptop ran out of batteries",
							"Wow, I would freeze",
							"Im trying to get g00d ;-;",
							"Thats not fair! I would have so killed you if you were not so decked",
							"Ha! I was recording haxor",
							"GG for the karma",
							"Sub to my channel - 'Huahwi' 4 my daily SG vids",
							"Sub to my channel - 'Technoblade' 4 my daily SG vids",
							"Sub to my channel - 'Pewdiepie' 4 my daily SG vids",
							"Sub to my channel - 'Fashion101' 4 my daily SG vids",
							"Teamers -,-",
							"I blame Technoblade.",
							"I blame Huahwi",
							"I blame RumbleCrumble",
							"I blame Obama",
							"Lucky crit",
							"I can report you to the cops for cyber bullying"};
	     plugin.getConfig().set("prizeBox.deathMessage.common", Arrays.asList(list5));
	     
	     String[] list6 = { "Oh come on, my kill aura should have hit you there!",
							"I promised myself I wouldnt cry",
							"Sub to my channel - 'Rumblecrumble' 4 my daily SG vids",
							"RIP",
							"You do not own me m9",
							"One day I want to be as good as you one day",
							"Camper!"};
	     plugin.getConfig().set("prizeBox.deathMessage.rare", Arrays.asList(list6));
	     
	     String[] list7 = { "Enjoy Carpal Tunnel Syndrome",
							"You probably see white and gold",
							"I sold my family for internet",
							"Ping-warrior :/",
							"Thats it, im quitting minecraft!!",
							"Tryhard",
							"At least I have a gf"};
	     plugin.getConfig().set("prizeBox.deathMessage.mythical", Arrays.asList(list7));
	     
	     String[] list8 = { "P2W",
							"Odds arent in my favor ;-;",
							"Very original strafes you got there",
							"Tell my wife shes fat!"};
	     plugin.getConfig().set("prizeBox.deathMessage.legendary", Arrays.asList(list8));
	     
	     String[] list9 = { "Im going to beat you until youre white and gold",
							"Ill have you know I play soup pvp.",
							"brb downloading more RAM",
							"my house is on fire, brb",
							"Is this the Krusty Krab?",
							"Your taunt message sucks.",
							"My taunt message is better.",
							"The mitochondria is the powerhouse of the cell.",
							"dont make me bring out my mixtape",
							"Oh look, a distraction!",
							"The keyboard is mightier than the sword.",
							"Your soul is a cavern of lies.",
							"The enemy of my enemy is also my enemy. Youre all terrible people.",
							"I hate every one of you equally.",
							"I hate every one of you equally.",
							"123 for team pls",
							"Trying is the first step towards failure.",
							"I really hope this isnt the only taunt I have unlocked.",
							"This is my best taunt, ok? Stop judging me!",
							"Je suis une pomme de terre",
							"Ha so funny -,-",
							"Youre really funny",
							"Youre really funny, nah jk lmao",
							"whoever im subscribed to is way better at pvp than whoever youre subscribed to",
							"Meanwhile in Canada",
							"WHY AM I ON FIRE?!",
							"Im too hot to die!",
							"I like SkyDoesMinecraft",
							"Butter!",
							"This is such a waste of a taunt.",
							"An eye for an eye and that trade was a massive waste of time.",
							"I hope Im on Youtube.",
							"Your skin is. beautiful! jk nerd",
							"I r8 8/8 m8 thats gr8. god im so original.",
							"(I sure hope Technoblade remembers to put an amazing taunt here)",
							"Does anyone know how to activate my taunt?",
							"This game is for little kids, Im going back to Roblox.",
							"Started from the bottom why am I still here?",
							"Taunting is for nerds.",
							"Hah! I bet you casuals dont even wear Gunnars.",
							"Every 60 seconds in Africa, a minute passes.",
							"Sometimes sloths mistake their arms for tree branches and then fall to their deaths.", 
							"Im throwing money at the screen, why hasnt a diamond sword appeared yet?",
							"This taunt is going to get really old once you hear it fifty times."};
	     plugin.getConfig().set("prizeBox.taunt.common", Arrays.asList(list9));
	     
	     String[] list10 = {"MOM GET THE CAMERA!",
							"Sometimes I like to bury myself in the garden and pretend I am a carrot.",
							"God I wish I had a better taunt message.",
							"god I wish I had something clever instead of this taunt.",
							"I won the Hunger Games and all I got was this lousy taunt.",
							"Why wont the voices stop?!",
							"Does anyone have anything original to contribute to the chat?",
							"Finally, a chance for my taunt to be relevant."};
	     plugin.getConfig().set("prizeBox.taunt.rare", Arrays.asList(list10));
	     
	     String[] list11 = {"Is making out with meteorites considered gay?",									
							"My favorite color is Wednesday.",																																
							"Life is like a box of chocolates, it doesnt last as long if youre fat.",
							"§5vOMG I FOUND BETTY",
							"You wont be laughing when the bananas rise up in rebellion.",
							"If at first you dont succeed, youre probably just really bad.",
							"(Amazing comeback)"};
	     plugin.getConfig().set("prizeBox.taunt.mythical", Arrays.asList(list11));
	     
	     String[] list12 = {"Your strafing is bad and you should feel bad.",
							"Say perhaps to drugs,",
							"(Witty retort)",
							"As a wise man once said: I am the best, you all suck, cant touch this."};
	     plugin.getConfig().set("prizeBox.taunt.legendary", Arrays.asList(list12));
	     
	     String[] list13 = {"[eZ]",
							"[g00d]",
							"[VIP]",
							"[n00b]",
							"[Scrub]",
							"[Nerfed]"};
	     plugin.getConfig().set("prizeBox.prefix.common", Arrays.asList(list13));
	     
	     String[] list14 = {"[MLG]",
							"[Master]",
							"[MVP]",
							"[Buffed]"};
	     plugin.getConfig().set("prizeBox.prefix.rare", Arrays.asList(list14));
	     
	     String[] list15 = {"[OpTiC]",			
							"[P2W]",
							"[OP]"};
	     plugin.getConfig().set("prizeBox.prefix.mythical", Arrays.asList(list15));
	     
	     String[] list16 = {"[FaZe]",
							"[TN]"};
	     plugin.getConfig().set("prizeBox.prefix.legendary", Arrays.asList(list16));
	     
	     String[] list17 = { "Wood",
							 "Brick"
							 "Birch-Wood Slab",
							 "Bookshelf",
							 "Brick Slab",
							 "Brick Stairs",
							 "Brown Mushroom",
							 "Brown Wool",
							 "Clay Block",
							 "Coal Ore",
							 "Cobblestone",
							 "Cobblestone Slab",
							 "Cobblestone Stairs",
							 "Cobblestone Wall",
							 "Diamond Ore",
							 "Dirt",
							 "Emerald Ore",
							 "End Portal",
							 "End Portal Frame",
							 "End Stone",
							 "Glowstone",
							 "Gold Ore",
							 "Grass",
							 "Gravel", 
							 "Hardened Clay",
							 "Hay Bale",
							 "Iron Ore",
							 "Jungle-Wood Slab",
							 "Lapis Lazuli Block",
							 "Lapis Lazuli Ore",
							 "Leaves",
							 "Lily Pad",
							 "Melon",
							 "Moss Stone",
							 "Mossy Cobblestone Wall",
							 "Mycelium",
							 "Nether Brick",
							 "Nether Brick Fence",
							 "Nether Brick Slab",
							 "Nether Brick Stairs",
							 "Nether Quartz Ore",
							 "Netherrack",
							 "Oak-Wood Slab",
							 "Portal",
							 "Quartz Block",
							 "Quartz Slab",
							 "Red Mushroom",
							 "Redstone Ore",
							 "Sand",
							 "Sandstone",
							 "Sandstone Slab",
							 "Sandstone Stairs",
							 "Snow",
							 "Snow Block",
							 "Soul Sand",
							 "Spruce-Wood Slab",
							 "Stained Clay",
							 "Stained Glass",
							 "Stained Glass Pane",
							 "Stone",
							 "Stone Brick Slab",
							 "Stone Brick Stairs",
							 "Stone Bricks",
							 "Stone Slab",
							 "Vines",
							 "Wooden Plank",
							 "Wooden Slab",
							 "Wooden Stairs"
							};
	     plugin.getConfig().set("prizeBox.hat.common", Arrays.asList(list17));
	     
	     String[] list18 = {"Sponge",
		 					"Black Wool",
							"Blue Wool",
							"Cyan Wool",
							"Glass Pane",
							"Fence",
							"Gray Wool",
 							"Green Wool",
							"Iron Bars",
							"Light Blue Wool",
							"Light Gray Wool",
							"Lime Wool",
							"Magenta Wool",
							"Orange Wool",
							"Pink Wool",
							"Purple Wool",
							"Red Wool",
							"Wool",
							"Yellow Wool"
		 };
	     plugin.getConfig().set("prizeBox.hat.rare", Arrays.asList(list18));
	     
	     String[] list19 = { "Obsidian",
							 "Bedrock",
							 "Cobweb",
		 					 "Fire",
							 "Glass",
							 "Ice",
							 "Head (Creeper)",
							 "Head (Skeleton)",
							 "Head (Steve)",
							 "Head (Wither)",
							 "Head (Zombie)",
							 "Jack-O-Lantern",
							 "Mob Spawner",
							 "Packed Ice",
							 "Pumpkin"
		 };
	     plugin.getConfig().set("prizeBox.hat.mythical", Arrays.asList(list19));
	     
	     String[] list20 = { "Glass",
		 					 "Block of Coal",
							 "Block of Diamond",
							 "Block of Emerald",
							 "Block of Gold",
							 "Block of Iron"
		 };
	     plugin.getConfig().set("prizeBox.hat.legendary", Arrays.asList(list20));
	     
	     String[] list21 = { "Paper",
							 "Paper",
							 "Paper",
							 "Name_tag",
							 "Glass"};
	     plugin.getConfig().set("prizeBox.spinTypes", Arrays.asList(list21));
	     
	     String[] list22 = { ChatColor.BLUE + "" + ChatColor.BOLD + "Win Message",
							 ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Death Message",
							 ChatColor.AQUA + "" + ChatColor.BOLD + "Taunt",
							 ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Chat Prefix",
							 ChatColor.GREEN + "" + ChatColor.BOLD + "Hat"};
	     plugin.getConfig().set("prizeBox.spinItems", Arrays.asList(list22));
	     
	     plugin.getConfig().options().copyDefaults(true);
	     plugin.saveConfig();
	}
}

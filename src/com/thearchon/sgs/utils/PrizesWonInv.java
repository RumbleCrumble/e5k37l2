package com.thearchon.sgs.utils;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PrizesWonInv {

	public void inv(Player p){
		Inventory inv = Bukkit.createInventory(null, 54, "�c�nPrizes Won");
		
		ItemStack bow = new ItemStack(Material.BOW);
		ItemStack prefix = new ItemStack(Material.NAME_TAG);
		ItemStack hat = new ItemStack(Material.GLASS);
		
		ItemStack winMessage = new ItemStack(Material.PAPER);
		ItemStack deathMessage = new ItemStack(Material.PAPER);
		ItemStack taunt = new ItemStack(Material.PAPER);
		
		ItemStack backArrow = new ItemStack(Material.ARROW);
		ItemStack common = new ItemStack(Material.WOOL,1,DyeColor.GREEN.getData());
		ItemStack rare = new ItemStack(Material.WOOL,1,DyeColor.CYAN.getData());
		ItemStack chest = new ItemStack(Material.CHEST);
		ItemStack mythical = new ItemStack(Material.WOOL,1,DyeColor.PURPLE.getData());
		ItemStack legendary = new ItemStack(Material.WOOL,1,DyeColor.YELLOW.getData());
		ItemStack nextArrow = new ItemStack(Material.ARROW);

	
		ItemMeta bowMeta = bow.getItemMeta();
		ItemMeta prefixMeta = prefix.getItemMeta();
		ItemMeta hatMeta = hat.getItemMeta();
		
		ItemMeta winMessageMeta = winMessage.getItemMeta();
		ItemMeta deathMessageMeta = deathMessage.getItemMeta();
		ItemMeta tauntMeta = taunt.getItemMeta();
		
		ItemMeta backArrowMeta = backArrow.getItemMeta();
		ItemMeta commonMeta = common.getItemMeta();
		ItemMeta rareMeta = rare.getItemMeta();
		ItemMeta chestMeta = chest.getItemMeta();
		ItemMeta mythicalMeta = mythical.getItemMeta();
		ItemMeta legendaryMeta = legendary.getItemMeta();
		ItemMeta nextArrowMeta = nextArrow.getItemMeta();
		
		
		bowMeta.setDisplayName(ChatColor.DARK_GREEN + "?");
		prefixMeta.setDisplayName(ChatColor.DARK_GREEN + "Prefixs");
		hatMeta.setDisplayName(ChatColor.GREEN + "Hats");
		
		winMessageMeta.setDisplayName(ChatColor.BLUE + "Win Messages");
		deathMessageMeta.setDisplayName(ChatColor.DARK_AQUA + "Death Messages");
		tauntMeta.setDisplayName(ChatColor.AQUA + "Taunts");
		
		backArrowMeta.setDisplayName(ChatColor.GREEN + "Go Back");
		commonMeta.setDisplayName(ChatColor.GREEN + "Common Rarity");
		rareMeta.setDisplayName(ChatColor.AQUA + "Rare Rarity");
		chestMeta.setDisplayName(ChatColor.RED + "Items");
		mythicalMeta.setDisplayName(ChatColor.DARK_PURPLE + "Mythical Rarity");
		legendaryMeta.setDisplayName(ChatColor.GOLD + "Legendary Rarity");
		nextArrowMeta.setDisplayName(ChatColor.GREEN + "Go Next");

		
		bowMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your ?."));
		prefixMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Prefixs."));
		hatMeta.setLore(Arrays.asList(ChatColor.GRAY + "your Hats."));
		
		winMessageMeta.setLore(Arrays.asList(ChatColor.GRAY + "your Win Messages."));
		deathMessageMeta.setLore(Arrays.asList(ChatColor.GRAY + "your Death Messages."));
		tauntMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Taunts."));

		backArrowMeta.setLore(Arrays.asList(ChatColor.GRAY + "a page."));
		commonMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Common Items."));
		rareMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Rare Items."));
		chestMeta.setLore(Arrays.asList(ChatColor.GRAY + "a page."));
		mythicalMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Mythical Items."));
		legendaryMeta.setLore(Arrays.asList(ChatColor.GRAY + "Your Legendary Items."));
		nextArrowMeta.setLore(Arrays.asList(ChatColor.GRAY + "a page."));

		
		bow.setItemMeta(bowMeta);
		prefix.setItemMeta(prefixMeta);
		hat.setItemMeta(hatMeta);
		
		winMessage.setItemMeta(winMessageMeta);
		deathMessage.setItemMeta(deathMessageMeta);
		taunt.setItemMeta(tauntMeta);

		backArrow.setItemMeta(backArrowMeta);
		common.setItemMeta(commonMeta);
		rare.setItemMeta(rareMeta);
		chest.setItemMeta(chestMeta);
		mythical.setItemMeta(mythicalMeta);
		legendary.setItemMeta(legendaryMeta);
		nextArrow.setItemMeta(nextArrowMeta);

		
		inv.setItem(1, bow);
		inv.setItem(2, prefix);
		inv.setItem(3, hat);
		
		inv.setItem(5, winMessage);
		inv.setItem(6, deathMessage);
		inv.setItem(7, taunt);
		
		inv.setItem(45, backArrow);
		inv.setItem(47, common);
		inv.setItem(48, rare);
		inv.setItem(49, chest);
		inv.setItem(50, mythical);
		inv.setItem(51, legendary);
		inv.setItem(53, nextArrow);

		
		p.openInventory(inv);
	}
	
}

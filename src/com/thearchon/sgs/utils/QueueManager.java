package com.thearchon.sgs.utils;

import java.util.ArrayList;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;

public class QueueManager {

	Core plugin = Core.getCore();
	
	public static ArrayList<ThePlayer> q = new ArrayList<ThePlayer>();
	
	ArrayList<ThePlayer> one = new ArrayList<ThePlayer>();
	ArrayList<ThePlayer> two = new ArrayList<ThePlayer>();
	ArrayList<ThePlayer> three = new ArrayList<ThePlayer>();
	
	ArrayList<Integer> closed = new ArrayList<Integer>();
	
	public static void q(ThePlayer p){
		
		if (q.contains(p)){
			q.remove(p);
		} else {
			q.add(p);
		}

	}
	
	public int getOpenServers(){
		
		if (!closed.contains(1)){
			return 1;
		}

		if (!closed.contains(2)){
			return 2;
		}

		if (!closed.contains(3)){
			return 3;
		}
		
		if (!closed.contains(4)){
			return 4;
		}
		
		if (!closed.contains(5)){
			return 5;
		}
		
		return 0;
	}
	
	public void send(){
		
		if (q.size() == 0){ // if there aren't any users inside of the queue
        	return; // get outa here!
        }
		
		for (ThePlayer p : q){

			 int serverid = getOpenServers();
			
			 ByteArrayDataOutput out = ByteStreams.newDataOutput();
			 out.writeUTF("Connect");
			 out.writeUTF("ffa");
			 //out.writeUTF("SG-" + serverid);
			 
			 //closed.add(serverid);
			
			 one.remove(p);
			 
			 p.getPlayer().sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
		}
	}
	
	public void match(){

        if (q.size() == 0){ // if there aren't any users inside of the queue
        	return; // get outa here!
        }

        for (ThePlayer p : q){ // for ALL of the users inside of the queue
	
        	/*if (q.size() <= 20 && q.size() >= 8){ // if there is less than 20 users in the queue, but more than 8...
        		q.remove(p);
        		one.add(p); // ...throw them into the queue one.
        		two.remove(p);
        		three.remove(p);
                return;
        	}*/
	
        	if (p.getPoints() >= 100){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 0-100...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.add(p); // throw them into queue one...
        		two.remove(p); // remove them from the other queues to pervent issues in the send method.
        		three.remove(p); // remove them from the other queues to pervent issues in the send method.
        		return;
        	}
	
        	if (p.getPoints() >= 250 && p.getPoints() <= 100){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 100-250...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.remove(p); // remove them from the other queues to pervent issues in the send method.
        		two.add(p); // throw them into queue two...
        		three.remove(p); // remove them from the other queues to pervent issues in the send method.
        		return;
        	}
	
        	if (p.getPoints() >= 500 && p.getPoints() <= 250){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 100-250...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.remove(p); // remove them from the other queues to pervent issues in the send method.
        		two.remove(p); // remove them from the other queues to pervent issues in the send method.
        		three.add(p); // throw them into queue three...
        		return;
        	}
        }
	}
	
}

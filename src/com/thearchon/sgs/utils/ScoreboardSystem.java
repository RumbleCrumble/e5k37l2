package com.thearchon.sgs.utils;

import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;

public class ScoreboardSystem {

	Core plugin = Core.getCore();
	
    private void clear(Scoreboard s){
    	s.getObjective("dummy").unregister();
    	
    	if (!s.getTeams().isEmpty()){
			s.getTeam("faze").unregister();
		}
    	
    	s.registerNewObjective("dummy", "dummy").setDisplaySlot(DisplaySlot.SIDEBAR);
    	
    }
	
    public void tournyscoreboard(ThePlayer p){
    	final Scoreboard sb = p.getPlayer().getScoreboard();
		
		clear(sb);
		
		final Objective o = sb.getObjective(DisplaySlot.SIDEBAR);
		
    	if (Storage.tourny == true){
			o.setDisplayName("§b§lpvp.thearchon.net");
			o.getScore("§d§lTourny").setScore(9);
			o.getScore("§a").setScore(8);
			o.getScore("§e§lTime").setScore(7);
			o.getScore("§c" + Storage.tmin + ":" + Storage.tsec).setScore(6);
			o.getScore("§fTournament").setScore(5);
			o.getScore("§c§lACTIVE!").setScore(4);
			o.getScore("§c").setScore(3);
			o.getScore("§c").setScore(2);
			o.getScore("§c").setScore(1);
			o.getScore("§c").setScore(0);
			p.getPlayer().setScoreboard(sb);
			
			return;
		} else if (Storage.tourny == false){
			o.setDisplayName("§b§lpvp.thearchon.net");
			o.getScore("§d§lTourny").setScore(6);
			o.getScore("§cN/A").setScore(5);
			o.getScore("§a").setScore(4);
			o.getScore("§f/tournament").setScore(3);
			o.getScore("§fto activate").setScore(2);
			o.getScore("§fa tourny.").setScore(1);
		}
    }
    
	public void ogscoreboard(ThePlayer p){
		
		final Scoreboard sb = p.getPlayer().getScoreboard();
		
		clear(sb);
		
		final Objective o = sb.getObjective(DisplaySlot.SIDEBAR);

		
		

			o.setDisplayName("§b§lpvp.thearchon.net");
			o.getScore("§c§lLeague").setScore(18);
			o.getScore(p.getLeague()).setScore(17);
			o.getScore("§9").setScore(16);
			o.getScore("§e§lStats").setScore(15);
			o.getScore("§fRank: §c#" + p.getRanking()).setScore(14);
			o.getScore("§fP: §c" + p.getPoints()).setScore(13);
			o.getScore("§fW: §c" + p.getWins()).setScore(12);
			o.getScore("§fL: §c" + p.getLosses()).setScore(11);
			o.getScore("§fKills: §c" + p.getKills()).setScore(10);
			o.getScore("§c").setScore(9);
			o.getScore("§a§lCoins").setScore(8);
			o.getScore("§c" + p.getCoins()).setScore(7);
			o.getScore("§a").setScore(6);
			o.getScore("§b§lServer").setScore(5);
			o.getScore("§fArchon SG").setScore(4);
		
			p.getPlayer().setScoreboard(sb);
			
			return;
		
	}
	
	/*
	 * 

&d&lTourny

&a&lTime Left
&f(Time left counter)

&e&lStats
&fPlace: (Tourney place GOES HERE)
&fPoints: (POINTS GO HERE for that time period)
&fW/L: (WINS/LOSSES GO HERE for that time period)
&fKills: (KILLS GO HERE for that time period)

&b&lTop 3
&f1. (1st in tourney)
&f2. (2nd in tourney)
&f3. (3rd in tourney)

	 */
}

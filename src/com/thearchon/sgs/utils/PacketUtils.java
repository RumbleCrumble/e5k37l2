package com.thearchon.sgs.utils;

import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.minecraft.server.v1_7_R4.PlayerConnection;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.spigotmc.ProtocolInjector;
import org.spigotmc.ProtocolInjector.PacketTitle;
import org.spigotmc.ProtocolInjector.PacketTitle.Action;

public class PacketUtils {

	private static boolean isPlayerRightVersion(Player player) {
        return ((CraftPlayer)player).getHandle().playerConnection.networkManager.getVersion() >= 47;
    }
	
	public static void sendTab(Player p, String head, String foot){
		if (isPlayerRightVersion(p)) {
            PlayerConnection connection = ((CraftPlayer)p).getHandle().playerConnection;
            IChatBaseComponent header = ChatSerializer.a("{\"text\": \"" + head +"\"}");
            IChatBaseComponent footer = ChatSerializer.a("{\"text\": \"" + foot +"\"}");
            connection.sendPacket(new ProtocolInjector.PacketTabHeader(header, footer));
        }
	}
	
	public static void sendTitle(Player p, String title, String subtitle, int fadeInTime, int fadeOutTime, int stayTime){
		if (isPlayerRightVersion(p)){
            IChatBaseComponent ctitle = ChatSerializer.a("{\"text\": \"" + title +"\"}");
            IChatBaseComponent csubtitle = ChatSerializer.a("{\"text\": \"" + subtitle +"\"}");
            
            PacketTitle ptitle = new PacketTitle(Action.TITLE, ctitle);
            PacketTitle psubtitle = new PacketTitle(Action.SUBTITLE, csubtitle);
            PacketTitle length = new PacketTitle(Action.TIMES, fadeInTime, stayTime, fadeOutTime);
            
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ptitle);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(psubtitle);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
		}
	}
	
	
}

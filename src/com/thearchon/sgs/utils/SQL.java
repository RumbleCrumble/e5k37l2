package com.thearchon.sgs.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.thearchon.sgs.sqldata.ConnectionDetails;

public class SQL {

	public Map<Player, Integer> kills = new HashMap<Player, Integer>();
	public Map<Player, Integer> wins = new HashMap<Player, Integer>();
	public Map<Player, Integer> losses = new HashMap<Player, Integer>();
	public Map<Player, Integer> ranking = new HashMap<Player, Integer>();
	public Map<Player, Integer> coins = new HashMap<Player, Integer>();
	
	String example = "1,3";
	
	public void test (){
		String[] data = example.split("\\,");
		
		for (int x = 0; x<data.length; x++){
			
		}
		
		System.out.print(data[1] + "\n" + data[2]);
		
	}
	
	public void hasItem(Player p){
		try {
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	private static Connection connection;
	
	private void rehandle(){
		try {
            Class.forName("com.mysql.jdbc.Driver");
          connection = DriverManager.getConnection(ConnectionDetails.detials);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
    public SQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
          connection = DriverManager.getConnection(ConnectionDetails.detials);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void takeCoins(Player p, int amount){
    	int i = getCoins(p);
    	
    	try {
    		
    		String s = "UPDATE `users` SET `coins` = ? WHERE `uuid` = ?";
    	
    			PreparedStatement statement = connection
                        .prepareStatement(s);
    			
    			statement.setInt(1, i-amount);
    			statement.setString(2, p.getUniqueId().toString());
    		
    			int value = 0;
    			
    			ResultSet rs = statement.executeQuery();
    			
    			if (rs.next()){
    				
    				value = rs.getInt("coins");
    				
    				
    				
    				rs.close();
    				statement.close();
    			
    				return;
    			
    			} else {
    				rs.close();
    				statement.close();
    			
    			
    				return;
    			}
    		
    		
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    	
    }
    
    public String getPrizes(Player p){
    	
    	try {
    		
    		String s = "SELECT `prizesWon` FROM `users` WHERE `uuid` = ?";
    	
    			PreparedStatement statement = connection
                        .prepareStatement(s);
    			
    			
    			statement.setString(1, p.getUniqueId().toString());
    		
    			String value = null;
    			
    			ResultSet rs = statement.executeQuery();
    			
    			if (rs.next()){
    				
    				value = rs.getString("prizesWon");
    				
    				rs.close();
    				statement.close();
    			
    				return value;
    			
    			} else {
    				rs.close();
    				statement.close();
    			
    			
    				return "FAILURE";
    			}
    		
    		
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILURE";
        }

    }
    
    public String addPrizes(Player p, int prize){
    	
    	try {
    		
    		String s = "UPDATE `users` SET `prizesWon` = ? WHERE `uuid` = ?";
    	
    			PreparedStatement statement = connection
                        .prepareStatement(s);
    			
    			statement.setString(1, getPrizes(p) + "," + prize);
    			statement.setString(2, p.getUniqueId().toString());
    		
    			String value = null;
    			
    			ResultSet rs = statement.executeQuery();
    			
    			if (rs.next()){
    				
    				value = rs.getString("prizesWon");
    				
    				rs.close();
    				statement.close();
    			
    				return value;
    			
    			} else {
    				rs.close();
    				statement.close();
    			
    			
    				return "FAILURE";
    			}
    		
    		
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILURE";
        }

    }

    public String getCurrentPrize(Player p){
    	try {
    		
    		String s = "SELECT `equipedPrize` FROM `users` WHERE `uuid` = ?";
    	
    			PreparedStatement statement = connection
                        .prepareStatement(s);

    			statement.setString(1, p.getUniqueId().toString());
    		
    			String value = null;
    			
    			ResultSet rs = statement.executeQuery();
    			
    			if (rs.next()){
    				
    				value = rs.getString("prizesWon");
    				
    				rs.close();
    				statement.close();
    			
    				return value;
    			
    			} else {
    				rs.close();
    				statement.close();
    			
    			
    				return "FAILURE";
    			}
    		
    		
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILURE";
        }
    }
    
    public int getCoins(Player p){
    	try {
    		
    		String s = "SELECT `coins` FROM `users` WHERE `uuid` = ?";
    	
    			PreparedStatement statement = connection
                        .prepareStatement(s);
    			
    			
    			statement.setString(1, p.getUniqueId().toString());
    		
    			int rank = 0;
    			
    			ResultSet rs = statement.executeQuery();
    			
    			if (rs.next()){
    				
    				rank = rs.getInt("rank");
    				
    				setRanking(p, rank);
    				
    				rs.close();
    				statement.close();
    			
    				return rank;
    			
    			} else {
    				rs.close();
    				statement.close();
    			
    			
    				return 0;
    			}
    		
    		
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public synchronized static boolean playerDataAvailible(Player p){
    	try {
    		
    		PreparedStatement statement = connection.prepareStatement("SELECT * FROM `users` WHERE `uuid` = ? ;");
    		
    		statement.setString(1, p.getUniqueId().toString());
    		
    		ResultSet result = statement.executeQuery();
    		
    		boolean contiansPlayer = result.next();
    		
    		return contiansPlayer;
    		
    	} catch (Exception e){
    		e.printStackTrace();
    		return false;
    	}
    	
    }
    
    public void fixData(Player p){
    	try {
    		
    		PreparedStatement statement;
    		
    		String query;
    		String id;
    		
    		query = "INSERT INTO  `users` (  `uuid` ,  `wins` ,  `points` ,  `losses` ,  `kills` ,  `ranking` ,  `name` ,  `tempKills` ) VALUES (?, 0, 0, 0, 0, 0, ?, 0);";
    		id = p.getUniqueId().toString();
    		
			statement = connection.prepareStatement(query);
			
			statement.setString(1, id);
			statement.setString(2, p.getName());
		
			statement.executeUpdate();
			
			this.getRanking(p);
			
			statement.close();
			
			return;
    	} catch (Exception e){
			e.printStackTrace();
			return;
		}
	}
    
    public void setName(Player p){
    	String s = "UPDATE `users` SET `name` = ? WHERE `uuid` = ?";
    	
    	try {
    		
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			statement.setString(1, p.getName());
			statement.setString(2, p.getUniqueId().toString());
			statement.executeUpdate();

			statement.close();
				
				return;
			
    	} catch (Exception e){
    		e.printStackTrace();
    		return;
    	}
    	
    	
    	
    }
    
    public void executeQuery(String q){
    	try {
    		
    		
			PreparedStatement statement = connection
                    .prepareStatement(q);
			

			statement.executeUpdate();

			statement.close();
				
			return;
			
    	} catch (Exception e){
    		e.printStackTrace();
    		return;
    	}
    }
    
    public String getThirdHighestPlayer(){
    	
    	String s = "SELECT `name` FROM `users` WHERE `ranking` = 3";
    	
    	try {
    		
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()){
				String hp = rs.getString("name");
				
				rs.close();
				statement.close();
				
				return hp;
			}
	
    	} catch (Exception e){
    		e.printStackTrace();
    		return "MHF_Question";
    	}
    	
    	return "MHF_Question";
    	
    }
    
    public String getSecondHighestPlayer(){
    	
    	String s = "SELECT `name` FROM `users` WHERE `ranking` = 2";
    	
    	try {
    		
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()){
				String hp = rs.getString("name");
				
				rs.close();
				statement.close();
			
				return hp;
			}
	
    	} catch (Exception e){
    		e.printStackTrace();
    		return "MHF_Question";
    	}
    	
    	return "MHF_Question";
    	
    }
    
    public String getHighestPlayer(){
    	
    	String s = "SELECT `name` FROM `users` WHERE `ranking` = 1";
    	
    	try {
    		
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()){
				String hp = rs.getString("name");
				
				rs.close();
				statement.close();
			
				return hp;
			}
	
    	} catch (Exception e){
    		e.printStackTrace();
    		return "MHF_Question";
    	}
    	
    	return "MHF_Question";
    	
    }
    
    private int setOfflineRanking(OfflinePlayer p, int i){
    	
    	try {
    		String s = "UPDATE `users` SET `ranking` = ? WHERE `uuid` = ?";
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			statement.setInt(1, i);
			statement.setString(2, p.getUniqueId().toString());
		
			int rank = 0;
			
			statement.executeUpdate();

			statement.close();
	
    	} catch (Exception e){
    		e.printStackTrace();
    		return 0;
    	}
    	
    	return 0;
    }
    
    private int setRanking(Player p, int i){
    	
    	try {
    		String s = "UPDATE `users` SET `ranking` = ? WHERE `uuid` = ?";
    		
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			statement.setInt(1, i);
			statement.setString(2, p.getUniqueId().toString());
		
			int rank = 0;
			
			statement.executeUpdate();

			statement.close();

    	} catch (Exception e){
    		e.printStackTrace();
    		return 0;
    	}
    	
    	return 0;
    }
    
    public int getOfflineRanking(OfflinePlayer p){
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
	
	
	try {
		
		String s = "SELECT uuid, wins, FIND_IN_SET( wins, (SELECT GROUP_CONCAT( wins ORDER BY wins DESC ) FROM users )) AS rank FROM users WHERE uuid =  ?";
	
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			
			statement.setString(1, p.getUniqueId().toString());
		
			int rank = 0;
			
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()){
				
				rank = rs.getInt("rank");
				
				setOfflineRanking(p, rank);
				
				rs.close();
				statement.close();
			
				return rank;
			
			} else {
				rs.close();
				statement.close();
				return 0;
			}
		
		
    } catch (Exception e) {
        e.printStackTrace();
        return 0;
    }
    }
    
    public int getRanking(Player p){
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
	
	
	try {
		
		String s = "SELECT uuid, wins, FIND_IN_SET( wins, (SELECT GROUP_CONCAT( wins ORDER BY wins DESC ) FROM users )) AS rank FROM users WHERE uuid =  ?";
	
			PreparedStatement statement = connection
                    .prepareStatement(s);
			
			
			statement.setString(1, p.getUniqueId().toString());
		
			int rank = 0;
			
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()){
				
				rank = rs.getInt("rank");
				
				setRanking(p, rank);
				
				rs.close();
				statement.close();
			
				return rank;
			
			} else {
				rs.close();
				statement.close();
			
			
				return 0;
			}
		
		
    } catch (Exception e) {
        e.printStackTrace();
        return 0;
    }
    }
    
    public int getWins(Player p) {
    	
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
    	String s = "SELECT `wins` FROM `users` WHERE `uuid` = ?;";
    	
    			try {
    	            PreparedStatement statement = connection
    	                    .prepareStatement(s);
    	            
    	            statement.setString(1, p.getUniqueId().toString());
    	            
    	            ResultSet result = statement.executeQuery();

    	            int wins;
    	            
    	            if (result.next()) {
    	            	wins = result.getInt("wins");
    	            	
    	            	 result.close();
    	            	 statement.close();

    	            	
    	                return wins;
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();
    	            	
    	                return 0;
    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	            return 0;
    	        }
    		}
 	
    public int updateSection(OfflinePlayer offlinePlayer, String column, int number) {
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
    			try {
    				
    				String s = "SELECT `?` FROM `users` WHERE `uuid` = ?;";
    				
    	            PreparedStatement statement = connection
    	                    .prepareStatement(s);
    	            
    	            statement.setString(1, column);
    	            statement.setString(2, offlinePlayer.getUniqueId().toString());
    	            
    	            ResultSet result = statement.executeQuery();

    	            int wins;
    	            
    	            if (result.next()) {
    	            	wins = result.getInt("losses");
    	            	
    	            	 result.close();
    	            	 statement.close();
    	            	
    	                return wins;
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();
    	            	
    	                return 0;
    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	            return 0;
    	        }
    		}
    
    public int getLosses(Player p) {
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
    			try {
    				
    				String s = "SELECT `losses` FROM `users` WHERE `uuid` = ?;";
    				
    	            PreparedStatement statement = connection
    	                    .prepareStatement(s);
    	            
    	            statement.setString(1, p.getUniqueId().toString());
    	            
    	            ResultSet result = statement.executeQuery();

    	            int wins;
    	            
    	            if (result.next()) {
    	            	wins = result.getInt("losses");
    	            	
    	            	 result.close();
    	                 statement.close();
    	            	
    	                return wins;
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	            	 statement.close();
    	            	
    	                return 0;
    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	            return 0;
    	        }
    		}
    
    public int getKills(Player p) {
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
    	String s = "SELECT `kills` FROM `users` WHERE `uuid` = ?;";
    	
    			try {
    				PreparedStatement statement = connection
                            .prepareStatement(s);
    	            
    	            statement.setString(1, p.getUniqueId().toString());
    	            
    	            ResultSet result = statement.executeQuery();

    	            int wins;
    	            
    	            if (result.next()) {
    	            	wins = result.getInt("kills");
    	            	
    	            	 result.close();
    	                 statement.close();
    	            	
    	                return wins;
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();
    	            	
    	                return 0;
    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	            return 0;
    	        }
    		}
   
    public void getStats(String who, Player p) {
    	
    	/*if (!playerDataAvailible(p)){
		fixData(p);
		return;
	}*/
    	String winq = "SELECT `wins` FROM `users` WHERE `name` = ?;";
    	String killsq = "SELECT `kills` FROM `users` WHERE `name` = ?;";
    	String lossesq = "SELECT `losses` FROM `users` WHERE `name` = ?;";
    	String rankingq = "SELECT `ranking` FROM `users` WHERE `name` = ?;";
    	
    	int wins = 0;
    	int kills = 0;
    	int losses = 0;
    	int ranking = 0;
    	int points = 0;
    	int leaguei = 0;
    	
    	String league = "";
    	
    	try {
            PreparedStatement statement = connection
                    .prepareStatement(killsq);
            
            statement.setString(1, who);
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
            	kills = result.getInt("kills");
            	
            	 result.close();
                 statement.close();


            } else {
            	
            	
            	 result.close();

            	
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    			try {
    	            PreparedStatement statement = connection
    	                    .prepareStatement(winq);
    	            
    	            statement.setString(1, who);
    	            
    	            ResultSet result = statement.executeQuery();
    	            
    	            if (result.next()) {
    	            	wins = result.getInt("wins");
    	            	
    	            	 result.close();
    	                 statement.close();

  
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();

    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	      
    	        }
    			
    			try {
    	            PreparedStatement statement = connection
    	                    .prepareStatement(lossesq);
    	            
    	            statement.setString(1, who);
    	            
    	            ResultSet result = statement.executeQuery();
    	            
    	            if (result.next()) {
    	            	losses = result.getInt("losses");
    	            	
    	            	 result.close();
    	                 statement.close();
  
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();


    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	      
    	        }
    			
    			try {
    	            PreparedStatement statement = connection
    	                    .prepareStatement(rankingq);
    	            
    	            statement.setString(1, who);
    	            
    	            ResultSet result = statement.executeQuery();
    	            
    	            if (result.next()) {
    	            	ranking = result.getInt("ranking");
    	            	
    	            	 result.close();
    	                 statement.close();
  
    	            } else {
    	            	
    	            	
    	            	 result.close();
    	                 statement.close();

    	            }
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	      
    	        }
    			
    			points = kills * 100 + wins * 1250 + losses * -50;
    			
    			if (points >= 250000){ // 250k+ diamond
    				leaguei = 4;
    			}
    			
    			if (points <= 250000 && points >= 100000){ // 250k gold
    				leaguei = 3;
    			}
    			
    			if (points <= 100000 && points >= 25000){ // 100k iron
    				leaguei = 2;
    			}
    			
    			if (points <= 25000){ // 25k leather
    				leaguei = 1;
    			}
    			
    			if (leaguei == 1){
    				league = "§7§lLeather";
    			}
    			
    			if (leaguei == 2){
    				league ="§f§lIron";
    			}
    			
    			if (leaguei == 3){
    				league = "§6§lGold";
    			}
    			
    			if (leaguei == 4){
    				league = "§b§lDiamond";
    			}
    			
    			p.sendMessage(Messages.prefix + "Grabing " + who + "'s stats, please wait...");
				p.sendMessage(Messages.prefix + "Name: §c§l" + who);
				p.sendMessage(Messages.prefix + "League: " + league);
				p.sendMessage("");
				p.sendMessage(Messages.prefix + "Ranking: §a#" + ranking);
				p.sendMessage(Messages.prefix + "Points: §a" + points);
				p.sendMessage(Messages.prefix + "Kills: §a" + kills);
				p.sendMessage(Messages.prefix + "Wins: §a" + wins);
				p.sendMessage(Messages.prefix + "Losses: §a" + losses);
    			
    		}
}



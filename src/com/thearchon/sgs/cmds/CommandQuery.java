package com.thearchon.sgs.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.CommandPerformEvent;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.Priority;
import com.thearchon.sgs.utils.Messages;

public class CommandQuery implements CommandExecutor {

	public boolean onCommand(CommandSender se, Command c, String l, String[] a){
		
		String q = "";
		
		if (c.getName().equalsIgnoreCase("query")){
			
			Player s = (Player) se;
			
			if (!s.isOp()){
				CommandPerformEvent e = new CommandPerformEvent(s, "/query", "OP");
				
				Bukkit.getServer().getPluginManager().callEvent(e);
				
				s.sendMessage(Messages.prefix + "§cNo permission.");
				return true;
			}
			
			if (a.length == 0){
				se.sendMessage(Messages.prefix + "§cPlease provide a(n) SQL query. Ex. UPDATE `users` SET `wins` = 69 WHERE `name` = 'TheTrollzYT'");
				return true;
			}
			
			for(int i = 0; i < a.length; i++){
	            String arg = a[i] + " ";
	            q = q + arg;
	        }
			
			se.sendMessage(Messages.prefix + "Executing query...");
			
			Core.getCore().getSQL().executeQuery(q);
		
			se.sendMessage(Messages.prefix + "Completed query: " + q);
			
		}
		
		return false;
	}
	
}

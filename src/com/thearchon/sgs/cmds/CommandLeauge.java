package com.thearchon.sgs.cmds;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.thearchon.sgs.utils.Messages;

public class CommandLeauge implements CommandExecutor {

	private void spawnMob(String league, Location loc){
		Zombie z = (Zombie) loc.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
		
		z.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999999, 256));
		
		if (league.equalsIgnoreCase("leather")){
			z.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
			z.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
			z.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
			z.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
			z.setCustomName("§7§lLeather");
			z.setBaby(false);
			return;
		}
		
		if (league.equalsIgnoreCase("Iron")){
			z.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
			z.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
			z.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
			z.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
			z.setCustomName("§f§lIron");
			return;
		}
		
		if (league.equalsIgnoreCase("Gold")){
			z.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
			z.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
			z.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
			z.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
			z.setCustomName("§6§lGold");
			return;
		}
		
		if (league.equalsIgnoreCase("Diamond")){
			z.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
			z.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
			z.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
			z.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
			z.setCustomName("§b§lDiamond");
			return;
		}
		
		z.setCustomName("§a§ka §c§lERROR §a§ka");
		
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("league") && sender.isOp()){
			if (args.length == 0){
				sender.sendMessage(Messages.prefix + "§cPlease supply a league.");
				return true;
			}
			
			Player p = (Player) sender;
			Location l = p.getLocation();
				
			spawnMob(args[0], l);
			
				
			
		}
		
		return false;
	}
	
	

}

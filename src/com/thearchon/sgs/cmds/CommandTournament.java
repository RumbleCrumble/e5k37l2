package com.thearchon.sgs.cmds;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.Storage;

public class CommandTournament implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("tournament") && sender.isOp()){
			sender.sendMessage(Messages.prefix + "Tournament started.");
			Storage.tourny = true;
		}
		
		return false;
	}
	
}

package com.thearchon.sgs.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;

public class CommandCoin implements CommandExecutor {
	
	public boolean onCommand(CommandSender s, Command c, String l, String[] a){
		
		if (!(s instanceof Player)){
			s.sendMessage("An error occurred.");
			return true;
		}
		
		Player p = (Player) s;
		ThePlayer tp = new ThePlayer(p);
		
		if (c.getName().equalsIgnoreCase("coins")){
			if (a.length == 0){
				tp.sendMessage(Messages.prefix + "§cSpecify arguments. /coins give [amount] <player>");
				return true;
			}
			
			if (a[0] == null){
				tp.sendMessage(Messages.prefix + "§cAn error occurred. The first argument is null. Please contact a developer ASAP.");
				return true;
			}
			
			if (a[1] == null){
				tp.sendMessage(Messages.prefix + "§cSpecify arguments. /coins give [amount] <player>");
				return true;
			}
			
			if (a[2] == null){
				tp.sendMessage(Messages.prefix + "§cSpecify arguments. /coins give [amount] <player>");
				return true;
			}
			
			if (a[0].equalsIgnoreCase("give")){
				Player t = Bukkit.getPlayer(a[2]);
				int am = Integer.parseInt(a[1]);
				
				if (t == null){
					tp.sendMessage(Messages.prefix + "§cThe player " + a[2] + "does not ");
					return true;
				}
				
				Core.getCore().getSQL().executeQuery("UPDATE `users` SET `coins` = " + am + " WHERE `uuid` = '" + t.getUniqueId() + "';");
				
			
				
			}
			
		}
		
		return false;
	}

}

package com.thearchon.sgs.cmds;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;

public class CommandStats implements CommandExecutor {

	//private ArrayList<Player> random = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		Player s = (Player) sender;
		
		ThePlayer p = new ThePlayer(s);
		
		if (cmd.getName().equalsIgnoreCase("stats")){
			if (args.length == 0){
				
				if (!(sender instanceof Player)){
					return false;
				}
				
				/*if (random.contains(s)){
					Core.getCore().getSQL().random(p);
					
					p.sendMessage(Messages.prefix + "Name: §c§l" + p.getName());
					p.sendMessage(Messages.prefix + "League: " + p.getLeague());
					p.sendMessage("");
					p.sendMessage(Messages.prefix + "Ranking: §c§lN/A");
					p.sendMessage(Messages.prefix + "Points: §a" + p.getPoints());
					p.sendMessage(Messages.prefix + "Kills: §a" + p.getKills(p));
					p.sendMessage(Messages.prefix + "Wins: §a" + p.getWins());
					p.sendMessage(Messages.prefix + "Losses: §a" + p.getLosses());
					return true;
				}*/
				
				p.sendMessage(Messages.prefix + "Grabing your stats, please wait...");
				//System.out.print("User " + p.getID().toString() + " checked their stats.");
				p.sendMessage(Messages.prefix + "Name: §c§l" + p.getName());
				p.sendMessage(Messages.prefix + "UUID: §c§l" + p.getID().toString());
				p.sendMessage(Messages.prefix + "League: " + p.getLeague());
				p.sendMessage("");
				p.sendMessage(Messages.prefix + "Ranking: §a#" + p.getRanking());
				//p.sendMessage(Messages.prefix + "Ranking: §aRanking is currently only availible on the SQL panel! :(");
				p.sendMessage(Messages.prefix + "Points: §a" + p.getPoints());
				p.sendMessage(Messages.prefix + "Kills: §a" + p.getKills());
				p.sendMessage(Messages.prefix + "Wins: §a" + p.getWins());
				p.sendMessage(Messages.prefix + "Losses: §a" + p.getLosses());
				
				
				return true;
			} else {
				Core.getCore().getSQL().getStats(args[0], s);
			}
				
			
			
			
			}
			
		
		
		return false;
	}
	
}

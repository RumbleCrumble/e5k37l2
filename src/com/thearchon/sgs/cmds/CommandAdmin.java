package com.thearchon.sgs.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.CommandPerformEvent;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.Priority;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.managers.AdminManager;
import com.thearchon.sgs.utils.Messages;

public class CommandAdmin implements CommandExecutor {

	Core plugin = Core.getCore();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		Player s = (Player) sender;
		
		ThePlayer p = new ThePlayer(s);
		
		if (cmd.getName().equalsIgnoreCase("admin")){
			
			if (!s.isOp()){
				CommandPerformEvent e = new CommandPerformEvent(s, "/admin", "OP");
				
				Bukkit.getServer().getPluginManager().callEvent(e);
				
				s.sendMessage(Messages.prefix + "§cNo permission.");
				return true;
			}
			
			p.sendMessage(Messages.prefix + "Opening §cAdmin §6menu...");
			new AdminManager().adminInventory(s);
		}
			
		
		
		return false;
	}
	
}
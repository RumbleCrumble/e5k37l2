package com.thearchon.sgs.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.CommandPerformEvent;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.Priority;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;

public class CommandBroadcast implements CommandExecutor {

	//private ArrayList<Player> random = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		Player s = (Player) sender;
		
		ThePlayer p = new ThePlayer(s);
		
		if (cmd.getName().equalsIgnoreCase("broadcast")){
			
			if (!s.isOp()){
				CommandPerformEvent e = new CommandPerformEvent(s, "/broadcast", "OP");
				
				Bukkit.getServer().getPluginManager().callEvent(e);
				
				s.sendMessage(Messages.prefix + "�cNo permission.");
				return true;
			}
			
			if (args.length == 0){
				
				if (!(sender instanceof Player)){
					return false;
				}
				
				
				p.sendMessage(Messages.prefix + "�cEnter a message.");
				
				return true;
			}
			
			String msg = "";
			
			for(int i = 0; i < args.length; i++){
	            String arg = args[i] + " ";
	            msg = msg + arg;
	        }
				Bukkit.broadcastMessage(Messages.prefix + "�6�l[BROADCAST]");
				Bukkit.broadcastMessage(Messages.prefix + msg.replace('&', '�'));
				Bukkit.broadcastMessage(Messages.prefix + "�6�l[BROADCAST]");
			
			}
			
		
		
		return false;
	}
	
}
package com.thearchon.sgs.cmds;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.thearchon.sgs.CommandPerformEvent;
import com.thearchon.sgs.Priority;
import com.thearchon.sgs.events.Inv;
import com.thearchon.sgs.utils.Messages;

public class CommandBypass implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("bypass")){
			Player p = (Player) sender;
				
			if (!p.isOp()){
				CommandPerformEvent e = new CommandPerformEvent(p, "/bypass", "OP");
				
				Bukkit.getServer().getPluginManager().callEvent(e);
				
				p.sendMessage(Messages.prefix + "§cNo permission.");
				return true;
			}
			
			if (Inv.bypass.contains(p)){
				Inv.bypass.remove(p);
				p.sendMessage(Messages.prefix + "Removed from bypass mode.");
				p.setGameMode(GameMode.ADVENTURE);
				
				ItemStack menu = new ItemStack(Material.COMPASS);
				
				ItemMeta menuMeta = menu.getItemMeta();
				
				menuMeta.setDisplayName("§6SG Menu");
				
				menuMeta.setLore(Arrays.asList("§9Right click to view the SG menu."));
				
				menu.setItemMeta(menuMeta);
				
				p.getPlayer().getInventory().setItem(4, menu);
				
			} else {
				Inv.bypass.add(p);
				p.sendMessage(Messages.prefix + "Added to bypass mode.");
				p.setGameMode(GameMode.CREATIVE);
				p.getInventory().clear();
			}
			
			
		}
		
		return false;
	}
	
}


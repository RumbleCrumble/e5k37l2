package com.thearchon.sgs.cmds;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thearchon.sgs.CommandPerformEvent;
import com.thearchon.sgs.Priority;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.Storage;

public class CommandKickall implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("kickall")){
			
			Player s = (Player) sender;
			
			if (!s.isOp()){
				CommandPerformEvent e = new CommandPerformEvent(s, "/kickall", "OP");
				
				Bukkit.getServer().getPluginManager().callEvent(e);
				
				s.sendMessage(Messages.prefix + "�cNo permission.");
				return true;
			}
			
			if (args.length == 0){
				sender.sendMessage(Messages.prefix + "�cPlease supply a kick message.");
				return true;
			}
			
			String msg = "";
			
			for(int i = 0; i < args.length; i++){
	            String arg = args[i] + " ";
	            msg = msg + arg;
	        }
			
			for (Player p : Storage.online){
				p.kickPlayer(Messages.prefix + "�c�lKICKED! \n �c�lReason: \n �9" + msg.replace('&', '�'));
			}
		}
		
		return false;
	}
	
	

}

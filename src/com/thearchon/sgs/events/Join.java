package com.thearchon.sgs.events;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.PacketUtils;
import com.thearchon.sgs.utils.SQL;
import com.thearchon.sgs.utils.ScoreboardSystem;
import com.thearchon.sgs.utils.Storage;

public class Join implements Listener {

	Core plugin = Core.getCore();
	
	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		plugin.getSQL();
		if (!SQL.playerDataAvailible(e.getPlayer())){
			e.disallow(Result.KICK_OTHER, Messages.prefix + "Fixing your data, just for you!");
			plugin.getSQL().fixData(e.getPlayer());
		}

	
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		plugin.getSQL().setName(e.getPlayer());
		
		Player np = e.getPlayer();
		
		plugin.getSQL().kills.put(np, plugin.getSQL().getKills(np));
		plugin.getSQL().wins.put(np, plugin.getSQL().getWins(np));
		plugin.getSQL().losses.put(np, plugin.getSQL().getLosses(np));
		plugin.getSQL().ranking.put(np, plugin.getSQL().getRanking(np));
		plugin.getSQL().coins.put(np, plugin.getSQL().getCoins(np));
		
		ThePlayer p = new ThePlayer(e.getPlayer());
		
		e.setJoinMessage(null);
		
		Storage.online.add(e.getPlayer());
		
        PacketUtils.sendTab(e.getPlayer(), "�c�lArchonSG \n �6Main Lobby", "�6A new kind of SG. \n �aOwners: RumbleCrumble, Huahwi, TheTrollzYT \n �cJoin the queue to begin!");
		
		Scoreboard s;
		Objective o;
		
		s = Bukkit.getScoreboardManager().getNewScoreboard();

		o = s.registerNewObjective("dummy", "dummy");

		o.setDisplayName("�6Loading...");

		o.setDisplaySlot(DisplaySlot.SIDEBAR);

		p.getPlayer().setFoodLevel(1000);
		
		e.getPlayer().setScoreboard(s);
		
		new ScoreboardSystem().ogscoreboard(p);
		
		p.spawn();
		p.sendMessage(Messages.prefix + "Welcome to ArchonSG!");
		p.getPlayer().setGameMode(GameMode.ADVENTURE);
		
		if (p.getLeague().contains("Leather") || p.getLeague().contains("N/A")){
			p.getPlayer().getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
			p.getPlayer().getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
			p.getPlayer().getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
			p.getPlayer().getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
		}
		
		if (p.getLeague().contains("Iron")){
			p.getPlayer().getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
			p.getPlayer().getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
			p.getPlayer().getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
			p.getPlayer().getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
		}
		
		if (p.getLeague().contains("Gold")){
			p.getPlayer().getInventory().setBoots(new ItemStack(Material.GOLD_BOOTS));
			p.getPlayer().getInventory().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
			p.getPlayer().getInventory().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
			p.getPlayer().getInventory().setHelmet(new ItemStack(Material.GOLD_HELMET));
		}
		
		if (p.getLeague().contains("Diamond")){
			p.getPlayer().getInventory().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
			p.getPlayer().getInventory().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
			p.getPlayer().getInventory().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
			p.getPlayer().getInventory().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
		}
		
		ItemStack chest = new ItemStack(Material.CHEST);
		ItemMeta chestMeta = chest.getItemMeta();
		chestMeta.setDisplayName("Prizes Won");
		chestMeta.setLore(Arrays.asList("�9Right click to view the cool prizes you have won."));
		chest.setItemMeta(chestMeta);
		
		ItemStack phead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta pheadMeta = (SkullMeta) phead.getItemMeta();
		pheadMeta.setDisplayName("Profile");
		pheadMeta.setLore(Arrays.asList("�6Your league: " + p.getLeague(), "�6Your wins: " + p.getWins(), "�6Your Kills: " + p.getKills(), "�6Your losses: " + p.getLosses(), "�6Your points: " + p.getPoints(), "�6Your ranking: #" + p.getRanking()));
		pheadMeta.setOwner(p.getName());
		phead.setItemMeta(pheadMeta);
		
		ItemStack sword = new ItemStack(Material.WOOD_SWORD);
		ItemMeta swordMeta = sword.getItemMeta();
		swordMeta.setDisplayName("Duel Challenger");
		swordMeta.setLore(Arrays.asList("�9Right click on a player to challenge them to a duel."));
		sword.setItemMeta(swordMeta);
		
		ItemStack dye = new ItemStack(Material.INK_SACK,1,(short)10);
		ItemMeta dyeMeta = dye.getItemMeta();
		dyeMeta.setDisplayName("Join Queue");
		dyeMeta.setLore(Arrays.asList("�9Right click to join the queue."));
		dye.setItemMeta(dyeMeta);
		
		ItemStack xp = new ItemStack(Material.NETHER_STAR);
		ItemMeta xpMeta = xp.getItemMeta();
		xpMeta.setDisplayName("Join Training Grounds");
		xpMeta.setLore(Arrays.asList("�9Right click to join the training grounds."));
		xp.setItemMeta(xpMeta);
		
		ItemStack nameTag = new ItemStack(Material.NAME_TAG);
		ItemMeta nameTagMeta = nameTag.getItemMeta();
		nameTagMeta.setDisplayName("0 Tournaments");
		nameTagMeta.setLore(Arrays.asList("�9/tournament for more info."));
		nameTag.setItemMeta(nameTagMeta);
		
		ItemStack tripwireHook = new ItemStack(Material.TRIPWIRE_HOOK);
		ItemMeta tripwireHookMeta = tripwireHook.getItemMeta();
		tripwireHookMeta.setDisplayName("0 Rare Keys");
		tripwireHookMeta.setLore(Arrays.asList("�9/rareKey for more info."));
		tripwireHook.setItemMeta(tripwireHookMeta);
		
		p.getPlayer().getInventory().setItem(0, chest);
		p.getPlayer().getInventory().setItem(1, phead);
		
		p.getPlayer().getInventory().setItem(3, sword);
		p.getPlayer().getInventory().setItem(4, dye);
		p.getPlayer().getInventory().setItem(5, xp);
		
		p.getPlayer().getInventory().setItem(7, nameTag);
		p.getPlayer().getInventory().setItem(8, tripwireHook);
	}
	
}

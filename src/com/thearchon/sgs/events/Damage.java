package com.thearchon.sgs.events;

import java.util.Arrays;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;

public class Damage implements Listener {

	@EventHandler
	public void onDeath(EntityDeathEvent e){
		Player p = (Player) e.getEntity();
		
		p.setGameMode(GameMode.ADVENTURE);
		p.getKiller().setGameMode(GameMode.ADVENTURE);
		
		p.getKiller().getInventory().clear();
		p.getInventory().clear();
		
		ItemStack menu = new ItemStack(Material.COMPASS);
		
		ItemMeta menuMeta = menu.getItemMeta();
		
		menuMeta.setDisplayName("§6SG Menu");
		
		menuMeta.setLore(Arrays.asList("§9Right click to view the SG menu."));
		
		menu.setItemMeta(menuMeta);
		
		p.getKiller().getInventory().setItem(4, menu);
		p.getInventory().setItem(4, menu);
		
		ThePlayer tpone = new ThePlayer(p);
		ThePlayer tptwo = new ThePlayer(p.getKiller());
		
		tpone.spawn();
		tptwo.spawn();
		
		tpone.sendMessage(Messages.prefix + "You lost the duel!");
		tptwo.sendMessage(Messages.prefix + "You won the duel!");
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e){
		
		if (Core.getCore().getDM().getInDuel().contains((Player) e.getEntity())){
			e.setCancelled(false);
			return;
		}
		
		e.setCancelled(true);
	}
	
}

package com.thearchon.sgs.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class Block implements Listener {

	@EventHandler
	public void onBreak(BlockBreakEvent e){
		if (!Inv.bypass.contains(e.getPlayer())){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if (!Inv.bypass.contains(e.getPlayer())){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
}

package com.thearchon.sgs.events;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.thearchon.sgs.Core;

public class PrizeBox implements Listener 
{
	Player player;
	Plugin plugin = Core.instance;
	Inventory inventory;
	
	@EventHandler
	public void onBlockClick(PlayerInteractEvent event)
	{
		if(event.getAction()==Action.RIGHT_CLICK_BLOCK)
		{
			if(event.getClickedBlock().getType() == Material.ENDER_CHEST)
			{
				event.setCancelled(true);
				player = event.getPlayer();
				player.sendMessage(ChatColor.GOLD + "You purchased a prize box spin for 50 Coins!");
				inventory = Bukkit.createInventory(null, 27, "Prize Box");
				player.openInventory(inventory);
				prizeBoxAnimation(0.0001);
			}
		}
	}
	
	public void prizeBoxAnimation(double delay)
	{
		for(int x = 0; x <27; x++)
		{
			inventory.setItem(x,new ItemStack(Material.STAINED_GLASS_PANE,1,(short)randInt(0,16)));
			if(x==13)
			{
				List<String> tempList = (List<String>) Core.instance.getConfig().getList("prizeBox.spinItems");
				int randVal = randInt(0,tempList.size());
				ItemStack stack = translateMaterial("prizeBox.spinTypes",randVal);
				ItemMeta meta = stack.getItemMeta();
				meta.setDisplayName(tempList.get(randVal));
				stack.setItemMeta(meta);
				inventory.setItem(x,stack);
				invDelay(delay);
			}
		}
	}
	
	public void finalPrize(String path)
	{
		List<String> tempList = (List<String>) plugin.getConfig().getList(path);
		int randVal = randInt(0,tempList.size());
		
		if(setRarity(randVal).equals("Mythical")||setRarity(randVal).equals("Legendary"))
			Bukkit.broadcastMessage("\n" + ChatColor.GRAY + "Player " + player.getDisplayName() + ChatColor.GRAY + " won a... " + "\n" + setRarityColor(setRarity(randVal)) + "" + ChatColor.BOLD + setRarity(randVal) + " " + getType(path) + ChatColor.RESET + setRarityColor(setRarity(randVal)) + ": " + tempList.get(randVal) + "\n");
		else
			player.sendMessage("\n" + ChatColor.GRAY + "You won a... " + "\n" + setRarityColor(setRarity(randVal)) + "" + ChatColor.BOLD + setRarity(randVal) + " " + getType(path) + ChatColor.RESET + setRarityColor(setRarity(randVal)) + ": " + tempList.get(randVal) + "\n");
	
		ItemStack stack = setFinalItem(path,randVal);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(setRarityColor(setRarity(randVal)) + "" + ChatColor.BOLD + setRarity(randVal) + " " + getType(path) + ChatColor.RESET + setRarityColor(setRarity(randVal)) + ": " + tempList.get(randVal));
		stack.setItemMeta(meta);
		for(int x = 0; x <27; x++)
		{
			inventory.setItem(x,new ItemStack(Material.AIR,1));
			if(x==13)
			{
				inventory.setItem(13,stack);
			}
		}
	}
	
	public String setRarity(int randVal)
	{
		if(randVal>0&&randVal<=750) //75%
		{
			return "Common";
		}
		else if(randVal>750&&randVal<=960) //21%
		{
			return "Rare";
		}
		else if(randVal>960&&randVal<=990) //3%
		{
			return "Mythical";
		}
		else //1%
		{
			return "Legendary";
		}
	}
		
	public ItemStack translateMaterial(String path, int randVal)
	{
		List<String> tempList = (List<String>) Core.instance.getConfig().getList(path);
		String toMaterial=tempList.get(randVal);
		toMaterial.toLowerCase.replace(" ", "_");
		Material m = Material.matchMaterial(toMaterial);
		return new ItemStack(m,1);
	}
	
	public int randInt(int low, int high)
	{
		return (int)(Math.random()*(high-low)+low);
	}
	
	public void invDelay(final double delay)
	{
		final int randVal = randInt(1,4);
		Bukkit.getScheduler().scheduleSyncDelayedTask(Core.instance, new Runnable(){
			public void run()
			{ 
				if(randVal==3||delay>0.75)
				{
					if(delay<1.0)
					{
						prizeBoxAnimation(delay*2);
					}
					else
					{
						finalPrize(setFinalPrizePath(setRarity(randInt(1,1001)),randInt(1,6)));
						launchFirework(player, 69);
					}
				}
				else
				{
					prizeBoxAnimation(delay);
				}			
			}
		},(long) (delay * 20L));
	}
	
	public String setFinalPrizePath(String rarity, int randVal)
	{
		while(randVal==1)
		{
			if(rarity.equals("Common"))
				return "prizeBox.winMessage.common";
			else if(rarity.equals("Rare"))
				return "prizeBox.winMessage.rare";
			else if(rarity.equals("Mythical"))
				return "prizeBox.winMessage.mythical";
			else
				return "prizeBox.winMessage.legendary";
		}
		while(randVal==2)
		{
			if(rarity.equals("Common"))
				return "prizeBox.deathMessage.common";
			else if(rarity.equals("Rare"))
				return "prizeBox.deathMessage.rare";
			else if(rarity.equals("Mythical"))
				return "prizeBox.deathMessage.mythical";
			else
				return "prizeBox.deathMessage.legendary";
		}
		while(randVal==3)
		{
			if(rarity.equals("Common"))
				return "prizeBox.taunt.common";
			else if(rarity.equals("Rare"))
				return "prizeBox.taunt.rare";
			else if(rarity.equals("Mythical"))
				return "prizeBox.taunt.mythical";
			else
				return "prizeBox.taunt.legendary";
		}
		while(randVal==4)
		{
			if(rarity.equals("Common"))
				return "prizeBox.prefix.common";
			else if(rarity.equals("Rare"))
				return "prizeBox.prefix.rare";
			else if(rarity.equals("Mythical"))
				return "prizeBox.prefix.mythical";
			else
				return "prizeBox.prefix.legendary";
		}
		while(randVal==5)
		{
			if(rarity.equals("Common"))
				return "prizeBox.hat.common";
			else if(rarity.equals("Rare"))
				return "prizeBox.hat.rare";
			else if(rarity.equals("Mythical"))
				return "prizeBox.hat.mythical";
			else
				return "prizeBox.hat.legendary";
		}
		return "";
	}
	
	public ItemStack setFinalItem(String path, int randVal)
	{
		if(path.substring(9,12).equals("hat"))
		{
			return translateMaterial(path, randVal);
		}
		else if(path.substring(9,15).equals("prefix"))
		{
			return new ItemStack(Material.NAME_TAG,1);
		}
		else
		{
			return new ItemStack(Material.PAPER,1);
		}
	}
	
	public void launchFirework(Player p, int speed) 
	{
		Firework fw = (Firework) p.getWorld().spawn(p.getEyeLocation(), Firework.class);
		fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
		FireworkMeta fwmeta = fw.getFireworkMeta();
		FireworkEffect.Builder builder = FireworkEffect.builder();
		builder.withTrail().withFlicker().withFade(Color.GREEN).withColor(Color.WHITE).withColor(Color.YELLOW)
		.withColor(Color.BLUE).withColor(Color.FUCHSIA).withColor(Color.PURPLE).withColor(Color.MAROON).withColor(Color.LIME)
		.withColor(Color.ORANGE).with(FireworkEffect.Type.BALL_LARGE);
		fwmeta.addEffect(builder.build());
		fwmeta.setPower(1);
		fw.setFireworkMeta(fwmeta);
	}
	
	public ChatColor setRarityColor(String rarity)
	{
		if(rarity.equals("Common"))
		{
			return ChatColor.GREEN;
		}
		else if(rarity.equals("Rare"))
		{
			return ChatColor.AQUA;
		}
		else if(rarity.equals("Mythical"))
		{
			return ChatColor.DARK_PURPLE;
		}
		else
		{
			return ChatColor.GOLD;
		}
	}
	
	public String getType(String path)
	{
		if(path.substring(9,12).equals("hat"))
		{
			return "Hat";
		}
		else if(path.substring(9,15).equals("prefix"))
		{
			return "Prefix";
		}
		else if(path.substring(9,14).equals("taunt"))
		{
			return "Taunt";
		}
		else if(path.substring(9,19).equals("winMessage"))
		{
			return "Win Message";
		}
		else
		{
			return "Death Message";
		}
	}
}
package com.thearchon.sgs.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.thearchon.sgs.handlers.ThePlayer;

public class Chat implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		
		
		
		
		ThePlayer p = new ThePlayer(e.getPlayer());
		
		
		if (e.getPlayer().isOp()){
			
			if (p.getLeague().equals("§7§lLeather")){
				e.setFormat("§c§lOwner §7| §7§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§f§lIron")){
				e.setFormat("§c§lOwner §7| §f§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§6§lGold")){
				e.setFormat("§c§lOwner §7| §6§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§b§lDiamond")){
				e.setFormat("§c§lOwner §7| §b§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
		} else {
			if (p.getLeague().equals("§7§lLeather")){
				e.setFormat("§7§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§f§lIron")){
				e.setFormat("§f§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§6§lGold")){
				e.setFormat("§6§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
			
			if (p.getLeague().equals("§b§lDiamond")){
				e.setFormat("§b§l" + e.getPlayer().getName() + " §7>> §r" + e.getMessage());
				return;
			}
		}
	}
	
}

package com.thearchon.sgs.events;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.PrizesWonInv;

public class Interact implements Listener {

	Core plugin = Core.getCore();
	
private void openLeagueInventory(Player p){
		
		ThePlayer tp = new ThePlayer(p);
		
		Inventory inv = Bukkit.createInventory(null, 54, "League Inventory");
		
		// 22 is the middle of this inventory
		// 20 = leather, 21 = iron, 22 = Player's league, 23 = gold, 24 = diamond
		// 13 = p skull
		ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemStack leather = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemStack iron = new ItemStack(Material.IRON_CHESTPLATE);
		ItemStack gold = new ItemStack(Material.GOLD_CHESTPLATE);
		ItemStack diamond = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemStack phead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		
		ItemMeta gMeta = glass.getItemMeta();
		ItemMeta leatherMeta = leather.getItemMeta();
		ItemMeta ironMeta = iron.getItemMeta();
		ItemMeta goldMeta = gold.getItemMeta();
		ItemMeta diamondMeta = diamond.getItemMeta();
		SkullMeta pheadMeta = (SkullMeta) phead.getItemMeta();
		
		gMeta.setDisplayName("|");
		leatherMeta.setDisplayName("Â§7Â§lLeather League â�Ÿ 0 - 25k points");
		ironMeta.setDisplayName("Â§fÂ§lIron League â�Ÿ 25k - 100k points");
		goldMeta.setDisplayName("Â§6Â§lGold League â�Ÿ 100k - 250k points");
		diamondMeta.setDisplayName("Â§bÂ§lDiamond League â�Ÿ 250k+ points");
		pheadMeta.setDisplayName("Â§9-= League Inventory =-");
		pheadMeta.setLore(Arrays.asList("Â§6Your league: " + tp.getLeague(), "Â§6Your wins: " + tp.getWins(), "Â§6Your Kills: " + tp.getKills(), "Â§6Your losses: " + tp.getLosses(), "Â§6Your points: " + tp.getPoints(), "Â§6Your ranking: #" + tp.getRanking()));
		pheadMeta.setOwner(p.getName());

		leatherMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		ironMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		goldMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		diamondMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		
		glass.setItemMeta(gMeta);
		leather.setItemMeta(leatherMeta);
		iron.setItemMeta(ironMeta);
		gold.setItemMeta(goldMeta);	
		diamond.setItemMeta(diamondMeta);
		phead.setItemMeta(pheadMeta);
	
		
		
		
		
		inv.setItem(20, leather);
		inv.setItem(21, iron);
		inv.setItem(22, phead);
		inv.setItem(23, gold);
		inv.setItem(24, diamond);
		
		tp.getPlayer().openInventory(inv);
	}
	
	private void openDuelInventory(Player p, String who){
		Inventory inv = Bukkit.createInventory(null, 9, who);
		
		ItemStack accept = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemStack deny = new ItemStack(Material.WOOL, 1, (byte) 14);
		ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
		
		ItemMeta acceptMeta = accept.getItemMeta();
		ItemMeta denyMeta = deny.getItemMeta();
		ItemMeta glassMeta = glass.getItemMeta();
		
		acceptMeta.setDisplayName("Â§aÂ§lAccept");
		denyMeta.setDisplayName("Â§cÂ§lDeny");
		glassMeta.setDisplayName("|");
		
		accept.setItemMeta(acceptMeta);
		deny.setItemMeta(denyMeta);
		glass.setItemMeta(glassMeta);
		
		for (int i = 0; i<inv.getSize(); i++){
			ItemStack it = inv.getItem(i);
			if (it==null||it.getType() == Material.AIR){
				inv.setItem(i, glass);
			}
		}
		inv.setItem(4, accept);
		inv.setItem(6, deny);
		
		p.openInventory(inv);
	}
	
	@EventHandler
	public void onPlayerEntityInteract(PlayerInteractEntityEvent e){
		
		Entity en = e.getRightClicked();
		
		
		
		if (en instanceof Player){
			
			Player clicked = (Player) en; // clicked player
			Player clicker = (Player) e.getPlayer(); // user who clicked on the clicked player
			
			if (plugin.getDM().getInDuel().contains(clicked)){
				clicker.sendMessage(Messages.prefix + "Â§cPlayer " + clicked.getCustomName() + " Â§cis already in a duel or has a duel request pending.");
				return;
			}
			
			
				
			/*if (plugin.getDM().getDuelData().containsValue(p)){
				ep.sendMessage(Messages.prefix + "Now dueling " + p.getName());
				p.sendMessage(Messages.prefix + "Now dueling " + ep.getName());
				p.teleport(new Location(Bukkit.getWorld("world"), -1201, 4, 1260));
				ep.teleport(new Location(Bukkit.getWorld("world"), -1215, 4, 1260));
				
				plugin.getDM().getDuelData().remove(ep, p);
				
				return;
			}*/
			
			clicker.sendMessage(Messages.prefix + "Sent duel request to " + clicked.getDisplayName() + "Â§6!");
			clicked.sendMessage(Messages.prefix + "You have a duel request from " + clicker.getDisplayName() + "Â§6.");
			
			this.openDuelInventory(clicked, clicker.getDisplayName());
			
			plugin.getDM().getDuelPending().add(clicker);
			plugin.getDM().getDuelPending().add(clicked);
		}
		
		if (e.getRightClicked() instanceof Zombie){
			Zombie z = (Zombie) en;

			if (z.getCustomName().contains("Leather")){
				openLeagueInventory(e.getPlayer());
			}
			
			if (z.getCustomName().contains("Iron")){
				openLeagueInventory(e.getPlayer());
			}

			if (z.getCustomName().contains("Gold")){
				openLeagueInventory(e.getPlayer());
			}

			if (z.getCustomName().contains("Diamond")){
				openLeagueInventory(e.getPlayer());
			}
			
			
		}
	}
	
	@EventHandler
	public void onRightClick(PlayerInteractEvent e){
		
		if (e.hasBlock()){
			return;
		}
		
		if(e.hasItem()) {
			ThePlayer p = new ThePlayer(e.getPlayer());
		
				
					if (e.getItem().getItemMeta().getDisplayName().equals("Prizes Won")){
						new PrizesWonInv().inv(e.getPlayer());
						p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BURP, 1, 1);
					}
					if (e.getItem().getItemMeta().getDisplayName().equals("Join Queue")){
						e.getPlayer().getInventory().setItem(4, new ItemStack(Material.AIR));

						ItemStack dye = new ItemStack(Material.INK_SACK,1,(short)1);
						ItemMeta dyeMeta = dye.getItemMeta();
						dyeMeta.setDisplayName("Leave Queue");
						dyeMeta.setLore(Arrays.asList("§9Right click to leave the queue."));
						dye.setItemMeta(dyeMeta);
						e.getPlayer().getInventory().setItem(4, dye);
						p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BURP, 1, 1);
					}
					if (e.getItem().getItemMeta().getDisplayName().equals("Leave Queue")){
						ItemStack dye = new ItemStack(Material.INK_SACK,1,(short)10);
						ItemMeta dyeMeta = dye.getItemMeta();
						dyeMeta.setDisplayName("Join Queue");
						dyeMeta.setLore(Arrays.asList("§9Right click to join the queue."));
						dye.setItemMeta(dyeMeta);
						e.getPlayer().getInventory().setItem(4, dye);
						p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BURP, 1, 1);
					}
					if (e.getItem().getItemMeta().getDisplayName().equals("Join Training Grounds")){
						e.getPlayer().teleport(new Location(Bukkit.getWorld("world"),-1352.500, 4, 1446.500));
						p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BURP, 1, 1);
					}
					if (e.getItem().getItemMeta().getDisplayName().equals("0 Tournaments")){
						e.getPlayer().performCommand("tournament");
						p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BURP, 1, 1);
					}
				}
			
		
		
	 }
	
}

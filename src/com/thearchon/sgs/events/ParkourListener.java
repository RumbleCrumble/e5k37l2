package com.thearchon.sgs.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.PacketUtils;

public class ParkourListener implements Listener {

	@EventHandler
	public void onPortalEnter(RegionEnterEvent e){
		if (e.getRegion().getId().equalsIgnoreCase("parkour1-end")){
			e.getPlayer().sendMessage(Messages.prefix + "Completed the first parkour!");
			ThePlayer p = new ThePlayer(e.getPlayer());
			
			p.spawn();
			PacketUtils.sendTitle(e.getPlayer(), "§6You beat the", "§afirst §6parkour!", 20, 60, 20);
		}
		
		if (e.getRegion().getId().equalsIgnoreCase("parkour2-end")){
			e.getPlayer().sendMessage(Messages.prefix + "Completed the second parkour!");
			ThePlayer p = new ThePlayer(e.getPlayer());
			
			p.spawn();
			PacketUtils.sendTitle(e.getPlayer(), "§6You beat the", "§asecond §6parkour!", 20, 60, 20);
		}
		
		if (e.getRegion().getId().equalsIgnoreCase("parkour3-end")){
			e.getPlayer().sendMessage(Messages.prefix + "Completed the third parkour!");
			ThePlayer p = new ThePlayer(e.getPlayer());
			
			p.spawn();
			PacketUtils.sendTitle(e.getPlayer(), "§6You beat the", "§athird §6parkour!", 20, 60, 20);
		}
		
		if (e.getRegion().getId().equalsIgnoreCase("parkour4-end")){
			e.getPlayer().sendMessage(Messages.prefix + "Completed the fourth parkour!");
			ThePlayer p = new ThePlayer(e.getPlayer());
			
			p.spawn();
			PacketUtils.sendTitle(e.getPlayer(), "§6You beat the", "§afourth §6parkour!", 20, 60, 20);
		}
		
		if (e.getRegion().getId().equalsIgnoreCase("lava")){
			e.getPlayer().sendMessage(Messages.prefix + "You failed...");
			ThePlayer p = new ThePlayer(e.getPlayer());
			
			p.spawn();
			PacketUtils.sendTitle(e.getPlayer(), "§6You failed...", "§6Such a shame...", 20, 60, 20);
		}
		
	}
	
}

package com.thearchon.sgs.events;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.managers.QueueManager;
import com.thearchon.sgs.utils.Messages;

public class Inv implements Listener {

	public static ArrayList<Player> bypass = new ArrayList<Player>();
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e){
		
		
		
//		if (e.getCurrentItem() == null) return;
		
		if (e.getCurrentItem().equals(null)){
			return;
		}
		
		if (e.getCurrentItem().getItemMeta() == null){
			return;
		}
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Accept")){
			Core.getCore().getDM().startDuel(p, Bukkit.getPlayer(e.getInventory().getName()));
		}
		
		if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Deny")){
			Core.getCore().getDM().getDuelPending().remove(p);
			Core.getCore().getDM().getDuelPending().remove(Bukkit.getPlayer(e.getInventory().getName()));
		}
		
		if (e.getInventory().getName().equals("§c§nSurvival Games Menu")){
			e.setCancelled(true);
			
			
			
			if (e.getCurrentItem().getItemMeta().getDisplayName().equals("§aSG Shop")){
				p.sendMessage(Messages.prefix + "§cComing soon...");
				p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 1, 1);
				p.closeInventory();
			}
			
			if (e.getCurrentItem().getItemMeta().getDisplayName().equals("§aJoin Queue")){
				p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 1, 1);
				p.sendMessage(Messages.prefix + "Added to the queue.");
				QueueManager.q(new ThePlayer(p));
				p.closeInventory();
			}
			
			if (e.getCurrentItem().getItemMeta().getDisplayName().equals("§cLeave Queue")){
				p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 1, 1);
				p.sendMessage(Messages.prefix + "Removed from the queue.");
				QueueManager.q(new ThePlayer(p));
				p.closeInventory();
			}
		}
		
		if (!bypass.contains(e.getWhoClicked())){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
	
	
}

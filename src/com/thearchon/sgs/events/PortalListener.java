package com.thearchon.sgs.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.utils.Messages;

public class PortalListener implements Listener {

	@EventHandler
	public void onPortalEnter(RegionEnterEvent e){
		if (e.getRegion().getId().equalsIgnoreCase("hubportal")){
			e.getPlayer().sendMessage(Messages.prefix + "Disconnecting...");
			
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("Connect");
		    out.writeUTF("lobby1");
			e.getPlayer().sendPluginMessage(Core.getCore(), "BungeeCord", out.toByteArray());
			
		}

		
	}
	
}

package com.thearchon.sgs.events;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.managers.QueueManager;
import com.thearchon.sgs.utils.Messages;

public class SignEvents implements Listener {

	@EventHandler
	public void onSignCreate(SignChangeEvent e){
		if(e.getLine(0).equalsIgnoreCase("[Join]")&&e.getLine(1).equalsIgnoreCase("Queue")&&e.getLine(2).equalsIgnoreCase("")){
			e.setLine(0, "§a[Join]");
			e.setLine(1, "Queue");
			e.setLine(2, "");
			e.setLine(3, "Click to join!");
		} else if (e.getLine(0).equalsIgnoreCase("[Leave]")&&e.getLine(1).equalsIgnoreCase("Queue")&&e.getLine(2).equalsIgnoreCase("")){
			e.setLine(0, "§c[Leave]");
			e.setLine(1, "Queue");
			e.setLine(2, "");
			e.setLine(3, "Click to leave!");
		}
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent e){
		ThePlayer p = new ThePlayer(e.getPlayer());
	 if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
		  if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
		    Sign sign = (Sign) e.getClickedBlock().getState();
		    if(sign.getLine(0).contains("[Join]")) {
		    	e.getPlayer().sendMessage(Messages.prefix + "Added into the queue.");
		    	QueueManager.q(p);
		    } else if (sign.getLine(0).contains("[Leave]")){
		    	e.getPlayer().sendMessage(Messages.prefix + "Removed from the queue.");
		    	QueueManager.q(p);
		    }
		  }
		}
	 }
}

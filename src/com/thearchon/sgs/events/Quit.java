package com.thearchon.sgs.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.managers.QueueManager;
import com.thearchon.sgs.utils.PacketUtils;
import com.thearchon.sgs.utils.SQL;
import com.thearchon.sgs.utils.Storage;

public class Quit implements Listener {

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Core.getCore().getQM().q.remove(new ThePlayer(e.getPlayer()));
		
		e.setQuitMessage(null);
		
		PacketUtils.sendTab(e.getPlayer(), "§c§lArchonSG \n §6League SG", "§6A new kind of SG. \n §aOwners: RumbleCrumble, Huahwi, TheTrollzYT \n §cJoin the queue to begin!");
		
		Storage.online.remove(e.getPlayer());
		
		Core.getCore().getSQL().kills.remove(e.getPlayer());
		Core.getCore().getSQL().wins.remove(e.getPlayer());
		Core.getCore().getSQL().ranking.remove(e.getPlayer());
		Core.getCore().getSQL().losses.remove(e.getPlayer());
		Core.getCore().getSQL().coins.remove(e.getPlayer());
	}
	
}

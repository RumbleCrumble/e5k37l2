package com.thearchon.sgs.events;



import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.managers.AdminManager;
import com.thearchon.sgs.managers.AdminStorage;
import com.thearchon.sgs.utils.Messages;

public class AdminListener implements Listener {

	@EventHandler
	public void click(InventoryClickEvent e){
		
		if (!e.getInventory().getName().equalsIgnoreCase("Admin Inventory")){
			return;
		}
		
		ItemStack i = e.getCurrentItem();
		
		ItemMeta im = i.getItemMeta();
		
		String iname = im.getDisplayName();
		
		Player p = (Player) e.getWhoClicked();
		
		if (iname.contains("Stats Modifier")){
			new AdminManager().statInventory(p);
		}
		
		if (iname.contains("Set")){
			if (AdminStorage.isBeingUsed == true){
				p.sendMessage(Messages.prefix + "Admin system not availible. Currently in use!");
				return;
			}
			
			if (p.isOp()){
				AdminStorage.pendingactionname.add(p);
				p.sendMessage(Messages.prefix + "Added into the list.");
				p.closeInventory();
				AdminStorage.isBeingUsed = true;
			}
			
		}
	}
	
	int i;
	OfflinePlayer of;
	
	@SuppressWarnings({ "deprecation" })
	@EventHandler
	public void chat(AsyncPlayerChatEvent e){
		
		Player p = e.getPlayer();
		
		if (AdminStorage.pendingactionname.contains(p)){
			
			AdminStorage.isBeingUsed = true;
			
			e.setCancelled(true);
			
			String name;
			name = e.getMessage();
			
			OfflinePlayer op = Bukkit.getOfflinePlayer(name);
			
			this.of = op;
			
			AdminStorage.pendingactionname.remove(p);
			
			Core.getCore().getSQL().updateSection(this.of, "wins", 1000);
			
			p.sendMessage(Messages.prefix + "Set the win count of " + op.getName() + " to 1000.");
			
			AdminStorage.isBeingUsed = false;
			
		} 
		
		/*if (AdminStorage.pendingactionvalue.contains(p) && this.of != null){
			e.setCancelled(true);
			
			p.sendMessage(Messages.prefix + "Added " + e.getMessage() + " into the selection.");
			
			this.i = Integer.parseInt(e.getMessage());
			
			Core.getCore().getSQL().updateSection(this.of, "wins", this.i);
			
			this.i = 0;
			this.of = null;
			
			AdminStorage.pendingactionvalue.remove(p);
			
			AdminStorage.isBeingUsed = false;
			
		}*/
	}
	
}

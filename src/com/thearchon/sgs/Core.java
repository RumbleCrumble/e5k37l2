package com.thearchon.sgs;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.thearchon.sgs.cmds.CommandAdmin;
import com.thearchon.sgs.cmds.CommandBroadcast;
import com.thearchon.sgs.cmds.CommandBypass;
import com.thearchon.sgs.cmds.CommandKickall;
import com.thearchon.sgs.cmds.CommandLeauge;
import com.thearchon.sgs.cmds.CommandQuery;
import com.thearchon.sgs.cmds.CommandStats;
import com.thearchon.sgs.cmds.CommandTournament;
import com.thearchon.sgs.events.AdminListener;
import com.thearchon.sgs.events.Block;
import com.thearchon.sgs.events.CPerform;
import com.thearchon.sgs.events.Chat;
import com.thearchon.sgs.events.Damage;
import com.thearchon.sgs.events.Interact;
import com.thearchon.sgs.events.Inv;
import com.thearchon.sgs.events.Join;
import com.thearchon.sgs.events.Leaves;
import com.thearchon.sgs.events.ParkourListener;
import com.thearchon.sgs.events.PortalListener;
import com.thearchon.sgs.events.PrizeBox;
import com.thearchon.sgs.events.Quit;
import com.thearchon.sgs.events.SignEvents;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.managers.DuelManager;
import com.thearchon.sgs.managers.LeaderboardManager;
import com.thearchon.sgs.managers.QueueManager;
import com.thearchon.sgs.utils.ConfigLoad;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.PacketUtils;
import com.thearchon.sgs.utils.SQL;
import com.thearchon.sgs.utils.ScoreboardState;
import com.thearchon.sgs.utils.ScoreboardSystem;
import com.thearchon.sgs.utils.Storage;


public class Core extends JavaPlugin {

	private static Core plugin;
	public static Plugin instance;
	
	private Map<UUID,Integer> tkills = new HashMap<UUID,Integer>();
	
	private SQL sql;
	
	public SQL getSQL(){
		return sql;
	}
	
	private QueueManager qm;
	
	private DuelManager dm;
	
	public QueueManager getQM(){
		return this.qm;
	}	
	
	public DuelManager getDM(){
		return this.dm;
	}
	
	public void onDisable(){
		for (Player p : Storage.online){
			p.kickPlayer(Messages.prefix + "�c�lKICKED! \n �c�lServer reloading... "
					+ "\n �c�lExplanation: The server method for getting "
					+ "\n �c�lall of the online players is broken with "
					+ "\n �c�lthis version of spigot/bukkit. "
					+ "\n �c�lTherefor, we needed our own method of "
					+ "\n �c�lgetting all of the online players."
					+ "\n �c�lThe only con is that our method removes "
					+ "\n �c�lthe online players after a reload."
					+ "\n �c�lAnd that, is why you are on this screen now."
					+ "\n �c�lTL;DR Something is broken.");
		}
	}
	
	public void onEnable(){
		plugin = this;
		instance = this;
		this.register();
		
		ConfigLoad.loadConfiguration();
		
		this.sql = new SQL();
		this.qm = new QueueManager();
		this.dm = new DuelManager();
		
		//LeaderboardManager.startHeadStuff();
		LeaderboardManager.startLeaderboard();
		
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		System.out.print("Loaded scoreboard task...");
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

			
			
			@Override
			public void run() {
				
				//System.out.print("Attempting to run task...");
				
				
				
				for (Player p : Storage.online){

					
					
					ThePlayer tp = new ThePlayer(p);
					
					//System.out.print("Checking current mode...");
					
					if (ScoreboardState.mode == 0){
						ScoreboardState.mode = 1;
						new ScoreboardSystem().tournyscoreboard(tp);
						//System.out.print("Set scoreboard to tourny mode.");
						return;
					} 
					if (ScoreboardState.mode == 1){
						ScoreboardState.mode = 0;
						//System.out.print("Set scoreboard to normal mode.");
						new ScoreboardSystem().ogscoreboard(tp);
						return;
					}
					
					
					
				}
			}
			
			
			
		}, 0, 100);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
			
			public void run(){
				
				if (Storage.tourny == true){
					
					for (Player p : Storage.online){
						PacketUtils.sendTab(p, "�c�lArchonSG \n �6Main Lobby", "�6A new kind of SG. \n �aOwners: RumbleCrumble, Huahwi, TheTrollzYT \n �cTournament active! �a�l" + Storage.tmin + "m" + Storage.tsec + "s");
					}
					
					if (Storage.tmin != 0 && Storage.tsec != 0){
						Storage.tsec--;
					}
					
					if (Storage.tsec == 0){
						Storage.tmin--;
						Storage.tsec = 59;
					}
					
					if (Storage.tmin == 0){
						Storage.tsec--;
					}
					
					if (Storage.tmin == 0 && Storage.tsec == 0){
						Storage.tmin = 59;
						Storage.tsec = 59;
						Storage.tourny = false;
					}
					
				}
				
				/*for (OfflinePlayer p : Bukkit.getOfflinePlayers()){
					getSQL().getOfflineRanking(p);
				}*/
				
				for (Player pl : Storage.online){
					
					ThePlayer p = new ThePlayer(pl);
					
					if (p.getLeague().contains("Leather") || p.getLeague().contains("N/A")){
						p.getPlayer().getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						p.getPlayer().getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						p.getPlayer().getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						p.getPlayer().getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
					}
					
					if (p.getLeague().contains("Iron")){
						p.getPlayer().getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
						p.getPlayer().getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						p.getPlayer().getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						p.getPlayer().getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
					}
					
					if (p.getLeague().contains("Gold")){
						p.getPlayer().getInventory().setBoots(new ItemStack(Material.GOLD_BOOTS));
						p.getPlayer().getInventory().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						p.getPlayer().getInventory().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						p.getPlayer().getInventory().setHelmet(new ItemStack(Material.GOLD_HELMET));
					}
					
					if (p.getLeague().contains("Diamond")){
						p.getPlayer().getInventory().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						p.getPlayer().getInventory().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						p.getPlayer().getInventory().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						p.getPlayer().getInventory().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
					}
				}
				
				for (Entity e : Bukkit.getWorld("world").getEntities()){
					if (e instanceof Zombie){
						Zombie z = (Zombie) e;
						Villager v = (Villager) e;
						
						z.setBaby(false);
						z.setVillager(false);
						
						if (z.getCustomName().contains("Leather")){
							z.teleport(new Location(Bukkit.getWorld("world"), -1229.500, 5, 1406.500));
						}
						
						if (z.getCustomName().contains("Iron")){
							z.teleport(new Location(Bukkit.getWorld("world"), -1227.500, 5, 1406.500));
						}

						if (z.getCustomName().contains("Gold")){
							z.teleport(new Location(Bukkit.getWorld("world"), -1225.500, 5, 1406.500));
						}
						if (z.getCustomName().contains("Diamond")){
							z.teleport(new Location(Bukkit.getWorld("world"), -1223.500, 5, 1406.500));
						
						if (v.getCustomName().contains("Shop")){
							v.teleport(new Location(Bukkit.getWorld("world"), -1224.500, 4.5, 1428.500));
						}
						}
					}
				}
			}
			
		}, 0, 20);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

			@Override
			public void run() {

				
				//qm.match();
				qm.send();
			}
			
			
			
		}, 0, 1200);
		
	}
	
	public static Core getCore(){
		return plugin;
	}
	
	private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
     
        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }
     
        return (WorldGuardPlugin) plugin;
    }
   
    private WorldEditPlugin getWorldEdit() {
            Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
             
        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldEditPlugin)) {
            return null; // Maybe you want throw an exception instead
        }
     
        return (WorldEditPlugin) plugin;
    }
	
	public void register(){
		// Events
		PluginManager pm = Bukkit.getServer().getPluginManager();
		
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new Quit(), this);
		pm.registerEvents(new Chat(), this);
		pm.registerEvents(new Inv(), this);
		pm.registerEvents(new SignEvents(), this);
		pm.registerEvents(new Damage(), this);
		pm.registerEvents(new Leaves(), this);
		pm.registerEvents(new Interact(), this);
		pm.registerEvents(new Block(), this);
		pm.registerEvents(new PrizeBox(), this);
		pm.registerEvents(new ParkourListener(), this);
		pm.registerEvents(new PortalListener(), this);
		pm.registerEvents(new AdminListener(), this);
		pm.registerEvents(new CPerform(), this);
		// Commands
		getCommand("stats").setExecutor(new CommandStats());
		getCommand("bypass").setExecutor(new CommandBypass());
		getCommand("broadcast").setExecutor(new CommandBroadcast());
		getCommand("kickall").setExecutor(new CommandKickall());
		getCommand("admin").setExecutor(new CommandAdmin());
		getCommand("league").setExecutor(new CommandLeauge());
		getCommand("tournament").setExecutor(new CommandTournament());
		getCommand("query").setExecutor(new CommandQuery());
	}
	
	/*@SuppressWarnings("deprecation")
	public void invDelay(final double delay, final Player p, final Inventory inv) {
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		public void run() {
		//p.openInventory(inv);
		MysteryBox.customInv(delay, p, inv);
		}
		}, (long) (delay * 20L));
	}*/

	public Map<UUID,Integer> getTkills() {
		return tkills;
	}
}

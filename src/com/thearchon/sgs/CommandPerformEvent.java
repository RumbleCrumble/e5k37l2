package com.thearchon.sgs;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CommandPerformEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	private Player p;
	private String action;
	private String perm;
	//private Priority pr;
	
	public CommandPerformEvent(Player p, String action, /*Priority pr,*/ String perm){
		p = this.p;
		action = this.action;
		//pr = this.pr;
		perm = this.perm;
	}
	
	//public Priority getPriority(){
	//	return this.pr;
	//}
	
	public Player getPlayer(){
		return this.p;
	}
	
	public String getAction(){
		return this.action;
	}
	
	public String getPermission(){
		return this.perm;
	}

}

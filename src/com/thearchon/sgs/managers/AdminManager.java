package com.thearchon.sgs.managers;

import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AdminManager {

	
	
	private void createItem(Inventory inv, int slot, Material material, int amount, int shrt, String displayName, String lore) {
		ItemStack item = new ItemStack(material, amount, (short) shrt);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName);
		meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
		inv.setItem(slot, item);
		return;
	}
	
	public void adminInventory(Player p){
		Inventory inv = Bukkit.createInventory(null, 9, "Admin Inventory");
		
		createItem(inv, 4, Material.ANVIL, 1, 0, "§cStats Modifier", "§5Click to modify a user's stats.");
		
		p.openInventory(inv);
	}
	
	public void statInventory(Player p){
		Inventory inv = Bukkit.createInventory(null, 9, "Stats Inventory");
		
		createItem(inv, 4, Material.ANVIL, 1, 0, "§cSet", "§5Click to set a user's wins.");
		
		p.openInventory(inv);
	}
	
}

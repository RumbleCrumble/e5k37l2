package com.thearchon.sgs.managers;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.thearchon.sgs.utils.Messages;

public class DuelManager {

	public void startDuel(Player one, Player two){
		one.teleport(new Location(Bukkit.getWorld("world"), -1201, 4, 1260));
		two.teleport(new Location(Bukkit.getWorld("world"), -1215, 4, 1260));
		one.sendMessage(Messages.prefix + "Now dueling " + two.getName());
		two.sendMessage(Messages.prefix + "Now dueling " + one.getName());
		this.duelPending.remove(one);
		this.duelPending.remove(two);
		this.inDuel.add(one);
		this.inDuel.add(one);
		
	}
	
	private ArrayList<Player> inDuel = new ArrayList<Player>();
	private ArrayList<Player> duelPending = new ArrayList<Player>();
 
 	
 	public ArrayList<Player> getInDuel(){
 		return this.inDuel;
 	}
 	
 	public ArrayList<Player> getDuelPending(){
 		return this.duelPending;
 	}
	
}

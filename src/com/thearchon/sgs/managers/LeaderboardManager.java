package com.thearchon.sgs.managers;


import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.SkullType;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;

import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;

public class LeaderboardManager {

	static Core plugin = Core.getCore();
	
	public static void startLeaderboard(){
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin,new Runnable(){

			@Override
			public void run() {
		
				
				
				Location hposone = new Location(Bukkit.getWorld("world"), -1247, 7, 1407);
				Location sposone = new Location(Bukkit.getWorld("world"), -1247, 6, 1408);
				
				Location hpostwo = new Location(Bukkit.getWorld("world"), -1246, 6, 1407);
				Location spostwo = new Location(Bukkit.getWorld("world"), -1246, 5, 1408);
				
				Location hposthree = new Location(Bukkit.getWorld("world"), -1248, 6, 1407);
				Location sposthree = new Location(Bukkit.getWorld("world"), -1248, 5, 1408);
			
				Block blockone = hposone.getBlock();
				Block signone = sposone.getBlock();
				
				Block blocktwo = hpostwo.getBlock();
				Block signtwo = spostwo.getBlock();
				
				Block blockthree = hposthree.getBlock();
				Block signthree = sposthree.getBlock();
				
				
				
				OfflinePlayer pone = Bukkit.getOfflinePlayer(plugin.getSQL().getHighestPlayer());
				OfflinePlayer ptwo = Bukkit.getOfflinePlayer(plugin.getSQL().getSecondHighestPlayer());
				OfflinePlayer pthree = Bukkit.getOfflinePlayer(plugin.getSQL().getThirdHighestPlayer());
				
				int i = 0;
				
				
				
				//if (blockone instanceof Skull && signone instanceof Sign){
				
					Skull s = (Skull) blockone.getState();
					
					String owner = pone.getName();
					
					s.setOwner(owner);
					
					s.update(true);
					
					Sign si = (Sign) signone.getState();
					
					si.setLine(2, owner);
					si.update();
					i++;
				
				
				
				
				//if (blocktwo instanceof Skull && signtwo instanceof Sign){
				
					Skull s2 = (Skull) blocktwo.getState();
					
					String owner2 = ptwo.getName();
					
					s2.setOwner(owner2);
					
					s2.update(true);
					
					Sign si2 = (Sign) signtwo.getState();
					
					si2.setLine(2, owner2);
					si2.update();
					i++;
				

				
				
				//if (blockthree instanceof Skull && signthree instanceof Sign){
					
					Skull s3 = (Skull) blockthree.getState();
					
					String owner3 = pthree.getName();
					
					s3.setOwner(owner3);
					
					s3.update(true);
					
					Sign si3 = (Sign) signthree.getState();
					
					si3.setLine(2, owner3);
					si3.update();
					i = 0;
				}
				
			
			
		}, 0, 20 * 60);
	}
	
	public static void startHeadStuff(){
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

			String one;
			String two;
			String three;
			
			
			
			@SuppressWarnings("static-access")
			@Override
			public void run() {
				
				
				
				Location hposone = new Location(Bukkit.getWorld("world"), -1247, 7, 1407);
				Location sposone = new Location(Bukkit.getWorld("world"), -1247, 6, 1408);
				
				Location hpostwo = new Location(Bukkit.getWorld("world"), -1246, 6, 1407);
				Location spostwo = new Location(Bukkit.getWorld("world"), -1247, 6, 1408);
				
				Location hposthree = new Location(Bukkit.getWorld("world"), -1248, 6, 1407);
				Location sposthree = new Location(Bukkit.getWorld("world"), -1248, 5, 1408);
			
				Block blockone = hposone.getBlock();
				Block signone = sposone.getBlock();
				
				Block blocktwo = hpostwo.getBlock();
				Block signtwo = spostwo.getBlock();
				
				Block blockthree = hposthree.getBlock();
				Block signthree = sposthree.getBlock();
				
				if (signone.getState() instanceof Sign){
					Sign sign = (Sign) signone.getState();
					
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getHighestPlayer());
		            
		            String n = p.getName();
		            this.one = p.getName();
					
		            int points = 0;
		            
		           
		            
					sign.setLine(2, p.getName());
					sign.setLine(3, "" + points);
					
					sign.update();
				}
				
				if (blockone.getState() instanceof Skull){
					
		            Skull skull = (Skull) blockone.getState();
		       
		            SkullType skullType = skull.getSkullType();
		            skullType = skullType.PLAYER;
		            
		          
		            
		            @SuppressWarnings("deprecation")
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getHighestPlayer());
		            
		            String n = this.one;
		            
		            skull.setOwner(p.getName());
		            skull.update(true, false);
		            
		            
		        }
				
				
				if (signtwo.getState() instanceof Sign){
					Sign sign = (Sign) signtwo.getState();
					
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getSecondHighestPlayer());
		            
		            String n = p.getName();
		            this.two = p.getName();
					
		            int points = 0;
		            
		            
					sign.setLine(2, p.getName());
					sign.setLine(3, "" + points);
					
					sign.update();
				}
				
				if (blocktwo.getState() instanceof Skull){
					
					
					
		            Skull skull = (Skull) blocktwo.getState();
		       
		            SkullType skullType = skull.getSkullType();
		            skullType = skullType.PLAYER;
		            
		          
		            
		            @SuppressWarnings("deprecation")
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getSecondHighestPlayer());
		            
		            String n = this.two;
		            
		            skull.setOwner(p.getName());
		           
		            
		            
		            
		            skull.update(true, false);
		            
		        }
				
				if (signthree.getState() instanceof Sign){
					Sign sign = (Sign) signthree.getState();
					
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getThirdHighestPlayer());
		            
		            String n = p.getName();
		            this.three = p.getName();
					
		            int points = 0;
		            
		            
		            
					sign.setLine(2, p.getName());
					sign.setLine(3, "" + points);
					
					sign.update();
				}
				
				if (blockthree.getState() instanceof Skull){
					
					
					
		            Skull skull = (Skull) blockthree.getState();
		       
		            SkullType skullType = skull.getSkullType();
		            skullType = skullType.PLAYER;
		            
		          
		            
		            @SuppressWarnings("deprecation")
					OfflinePlayer p = Bukkit.getOfflinePlayer(plugin.getSQL().getThirdHighestPlayer());
		            
		            String n = this.three;
		            
		            skull.setOwner(p.getName());
		           
		            skull.update(true, false);
		            
		        }
				
			}
			
			
		}, 0, 20);
	}
	
}

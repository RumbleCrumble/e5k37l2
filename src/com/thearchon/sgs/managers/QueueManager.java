package com.thearchon.sgs.managers;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thearchon.sgs.Core;
import com.thearchon.sgs.handlers.ThePlayer;
import com.thearchon.sgs.utils.Messages;
import com.thearchon.sgs.utils.Storage;

public class QueueManager {

	Core plugin = Core.getCore();
	
	public static ArrayList<ThePlayer> q = new ArrayList<ThePlayer>();
	
	ArrayList<ThePlayer> one = new ArrayList<ThePlayer>();
	ArrayList<ThePlayer> two = new ArrayList<ThePlayer>();
	ArrayList<ThePlayer> three = new ArrayList<ThePlayer>();
	
	ArrayList<Integer> closed = new ArrayList<Integer>();
	
	public static void q(ThePlayer p){
		
		if (q.contains(p)){
			q.remove(p);
		} else {
			q.add(p);
		}

	}
	
	private void sendToServers(){
		for (ThePlayer p : q){

        	p.sendMessage(Messages.prefix + "§e§lConnecting to a server...");
        	
			/* int serverid = getOpenServers();
			
			 ByteArrayDataOutput out = ByteStreams.newDataOutput();
			 out.writeUTF("Connect");
			 out.writeUTF("ffa");
			 //out.writeUTF("SG-" + serverid);
			 
			 //closed.add(serverid);
			
			 one.remove(p);
			 
			 p.getPlayer().sendPluginMessage(plugin, "BungeeCord", out.toByteArray());*/
        	p.sendMessage(Messages.prefix + "§c§lServer connection proccess cancelled.");
        	q.clear();
        	one.clear();
        	two.clear();
        	three.clear();
		}
	}
	
	private int getOpenServers(){
		
		if (!closed.contains(1)){
			return 1;
		}

		if (!closed.contains(2)){
			return 2;
		}

		if (!closed.contains(3)){
			return 3;
		}
		
		if (!closed.contains(4)){
			return 4;
		}
		
		if (!closed.contains(5)){
			return 5;
		}
		
		return 0;
	}
	
	public void send(){
		
		if (q.size() == 0){ // if there aren't any users inside of the queue
			
			for (Player p : Storage.online){
				p.sendMessage(Messages.prefix + "§c§lATTENTION: The queue is empty!");
			}
			
        	return; // get outa here!
        }

        for (ThePlayer p : q){ // for ALL of the users inside of the queue
	
        	if (q.size() >= 1){ // if there is less than 20 users in the queue, but more than 8...
        		q.remove(p);
        		p.sendMessage(Messages.prefix + "Error: Not enough people inside of the queue.");
                return;
        	}
        	
        	if (q.size() <= 20 && q.size() >= 8){ // if there is less than 20 users in the queue, but more than 8...
        		q.remove(p);
        		one.add(p); // ...throw them into the queue one.
        		two.remove(p);
        		three.remove(p);
        		p.sendMessage(Messages.prefix + "You were added into queue one. Please wait, we are now locating a server.");
        		sendToServers();
                return;
        	}
	
        	if (p.getPoints() >= 100){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 0-100...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.add(p); // throw them into queue one...
        		two.remove(p); // remove them from the other queues to pervent issues in the send method.
        		three.remove(p); // remove them from the other queues to pervent issues in the send method.
        		p.sendMessage(Messages.prefix + "You were added into queue one. Please wait, we are now locating a server.");
        		sendToServers();
        		return;
        	}
	
        	if (p.getPoints() >= 250 && p.getPoints() <= 100){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 100-250...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.remove(p); // remove them from the other queues to pervent issues in the send method.
        		two.add(p); // throw them into queue two...
        		three.remove(p); // remove them from the other queues to pervent issues in the send method.
        		p.sendMessage(Messages.prefix + "You were added into queue two. Please wait, we are now locating a server.");
        		sendToServers();
        		return;
        	}
	
        	if (p.getPoints() >= 500 && p.getPoints() <= 250){ // Obviously, the points wouldn't be this low, but we check if their points are within a range of 250-500...
        		q.remove(p); // remove them from the queue as they have been 'served'
        		one.remove(p); // remove them from the other queues to pervent issues in the send method.
        		two.remove(p); // remove them from the other queues to pervent issues in the send method.
        		three.add(p); // throw them into queue three...
        		p.sendMessage(Messages.prefix + "You were added into queue three. Please wait, we are now locating a server.");
        		sendToServers();
        		return;
        	}

		
	}
	
        
        
	}
	
}
